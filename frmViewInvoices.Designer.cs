﻿
namespace Invoicing_Application
{
    partial class frmViewInvoices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxConsoleLog = new System.Windows.Forms.TextBox();
            this.labelMessageLog = new System.Windows.Forms.Label();
            this.textBoxSubtotal = new System.Windows.Forms.TextBox();
            this.textBoxVAT = new System.Windows.Forms.TextBox();
            this.textBoxFinalTotal = new System.Windows.Forms.TextBox();
            this.textBoxPaymentStatus = new System.Windows.Forms.TextBox();
            this.textBoxJobIDValue = new System.Windows.Forms.TextBox();
            this.textBoxPayerID = new System.Windows.Forms.TextBox();
            this.textBoxPayeeIDValue = new System.Windows.Forms.TextBox();
            this.dateTimePickerInvoiceDueDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.labelInvoiceIDValue = new System.Windows.Forms.Label();
            this.labelFinalTotal = new System.Windows.Forms.Label();
            this.labelVAT = new System.Windows.Forms.Label();
            this.labelPaymentStatus = new System.Windows.Forms.Label();
            this.labelSubtotal = new System.Windows.Forms.Label();
            this.labelJobID = new System.Windows.Forms.Label();
            this.labelPayerID = new System.Windows.Forms.Label();
            this.labelPayeeID = new System.Windows.Forms.Label();
            this.labelInvoiceDueDate = new System.Windows.Forms.Label();
            this.labelInvoiceDate = new System.Windows.Forms.Label();
            this.helpMenuStrip = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelInvoiceID = new System.Windows.Forms.Label();
            this.buttonRefreshRecords = new System.Windows.Forms.Button();
            this.labelListOfInvoices = new System.Windows.Forms.Label();
            this.buttonEditInvoicePaymentStatus = new System.Windows.Forms.Button();
            this.dataGridViewListofInvoices = new System.Windows.Forms.DataGridView();
            this.labelClientName = new System.Windows.Forms.Label();
            this.labelJobName = new System.Windows.Forms.Label();
            this.labelFinalTotalValue = new System.Windows.Forms.Label();
            this.labelVATValue = new System.Windows.Forms.Label();
            this.labelSubtotalValue = new System.Windows.Forms.Label();
            this.helpMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListofInvoices)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxConsoleLog
            // 
            this.textBoxConsoleLog.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxConsoleLog.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxConsoleLog.Location = new System.Drawing.Point(249, 256);
            this.textBoxConsoleLog.Multiline = true;
            this.textBoxConsoleLog.Name = "textBoxConsoleLog";
            this.textBoxConsoleLog.ReadOnly = true;
            this.textBoxConsoleLog.Size = new System.Drawing.Size(548, 188);
            this.textBoxConsoleLog.TabIndex = 51;
            // 
            // labelMessageLog
            // 
            this.labelMessageLog.AutoSize = true;
            this.labelMessageLog.Location = new System.Drawing.Point(246, 240);
            this.labelMessageLog.Name = "labelMessageLog";
            this.labelMessageLog.Size = new System.Drawing.Size(71, 13);
            this.labelMessageLog.TabIndex = 50;
            this.labelMessageLog.Text = "Message Log";
            // 
            // textBoxSubtotal
            // 
            this.textBoxSubtotal.Location = new System.Drawing.Point(111, 204);
            this.textBoxSubtotal.Name = "textBoxSubtotal";
            this.textBoxSubtotal.ReadOnly = true;
            this.textBoxSubtotal.Size = new System.Drawing.Size(100, 20);
            this.textBoxSubtotal.TabIndex = 49;
            this.textBoxSubtotal.Visible = false;
            // 
            // textBoxVAT
            // 
            this.textBoxVAT.Location = new System.Drawing.Point(111, 233);
            this.textBoxVAT.Name = "textBoxVAT";
            this.textBoxVAT.ReadOnly = true;
            this.textBoxVAT.Size = new System.Drawing.Size(100, 20);
            this.textBoxVAT.TabIndex = 48;
            this.textBoxVAT.Visible = false;
            // 
            // textBoxFinalTotal
            // 
            this.textBoxFinalTotal.Location = new System.Drawing.Point(111, 265);
            this.textBoxFinalTotal.Name = "textBoxFinalTotal";
            this.textBoxFinalTotal.ReadOnly = true;
            this.textBoxFinalTotal.Size = new System.Drawing.Size(100, 20);
            this.textBoxFinalTotal.TabIndex = 47;
            this.textBoxFinalTotal.Visible = false;
            // 
            // textBoxPaymentStatus
            // 
            this.textBoxPaymentStatus.Location = new System.Drawing.Point(111, 295);
            this.textBoxPaymentStatus.Name = "textBoxPaymentStatus";
            this.textBoxPaymentStatus.Size = new System.Drawing.Size(100, 20);
            this.textBoxPaymentStatus.TabIndex = 46;
            // 
            // textBoxJobIDValue
            // 
            this.textBoxJobIDValue.Location = new System.Drawing.Point(111, 173);
            this.textBoxJobIDValue.Name = "textBoxJobIDValue";
            this.textBoxJobIDValue.ReadOnly = true;
            this.textBoxJobIDValue.Size = new System.Drawing.Size(89, 20);
            this.textBoxJobIDValue.TabIndex = 45;
            // 
            // textBoxPayerID
            // 
            this.textBoxPayerID.Location = new System.Drawing.Point(111, 148);
            this.textBoxPayerID.Name = "textBoxPayerID";
            this.textBoxPayerID.ReadOnly = true;
            this.textBoxPayerID.Size = new System.Drawing.Size(89, 20);
            this.textBoxPayerID.TabIndex = 44;
            // 
            // textBoxPayeeIDValue
            // 
            this.textBoxPayeeIDValue.Location = new System.Drawing.Point(111, 121);
            this.textBoxPayeeIDValue.Name = "textBoxPayeeIDValue";
            this.textBoxPayeeIDValue.ReadOnly = true;
            this.textBoxPayeeIDValue.Size = new System.Drawing.Size(100, 20);
            this.textBoxPayeeIDValue.TabIndex = 43;
            // 
            // dateTimePickerInvoiceDueDate
            // 
            this.dateTimePickerInvoiceDueDate.Enabled = false;
            this.dateTimePickerInvoiceDueDate.Location = new System.Drawing.Point(111, 97);
            this.dateTimePickerInvoiceDueDate.Name = "dateTimePickerInvoiceDueDate";
            this.dateTimePickerInvoiceDueDate.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerInvoiceDueDate.TabIndex = 42;
            // 
            // dateTimePickerInvoiceDate
            // 
            this.dateTimePickerInvoiceDate.Enabled = false;
            this.dateTimePickerInvoiceDate.Location = new System.Drawing.Point(87, 66);
            this.dateTimePickerInvoiceDate.Name = "dateTimePickerInvoiceDate";
            this.dateTimePickerInvoiceDate.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerInvoiceDate.TabIndex = 41;
            // 
            // labelInvoiceIDValue
            // 
            this.labelInvoiceIDValue.AutoSize = true;
            this.labelInvoiceIDValue.Location = new System.Drawing.Point(75, 41);
            this.labelInvoiceIDValue.Name = "labelInvoiceIDValue";
            this.labelInvoiceIDValue.Size = new System.Drawing.Size(101, 13);
            this.labelInvoiceIDValue.TabIndex = 40;
            this.labelInvoiceIDValue.Text = "*Placeholder Value*";
            // 
            // labelFinalTotal
            // 
            this.labelFinalTotal.AutoSize = true;
            this.labelFinalTotal.Location = new System.Drawing.Point(25, 269);
            this.labelFinalTotal.Name = "labelFinalTotal";
            this.labelFinalTotal.Size = new System.Drawing.Size(56, 13);
            this.labelFinalTotal.TabIndex = 39;
            this.labelFinalTotal.Text = "Final Total";
            // 
            // labelVAT
            // 
            this.labelVAT.AutoSize = true;
            this.labelVAT.Location = new System.Drawing.Point(47, 236);
            this.labelVAT.Name = "labelVAT";
            this.labelVAT.Size = new System.Drawing.Size(34, 13);
            this.labelVAT.TabIndex = 38;
            this.labelVAT.Text = "+VAT";
            // 
            // labelPaymentStatus
            // 
            this.labelPaymentStatus.AutoSize = true;
            this.labelPaymentStatus.Location = new System.Drawing.Point(0, 297);
            this.labelPaymentStatus.Name = "labelPaymentStatus";
            this.labelPaymentStatus.Size = new System.Drawing.Size(81, 13);
            this.labelPaymentStatus.TabIndex = 37;
            this.labelPaymentStatus.Text = "Payment Status";
            // 
            // labelSubtotal
            // 
            this.labelSubtotal.AutoSize = true;
            this.labelSubtotal.Location = new System.Drawing.Point(35, 207);
            this.labelSubtotal.Name = "labelSubtotal";
            this.labelSubtotal.Size = new System.Drawing.Size(46, 13);
            this.labelSubtotal.TabIndex = 36;
            this.labelSubtotal.Text = "Subtotal";
            // 
            // labelJobID
            // 
            this.labelJobID.AutoSize = true;
            this.labelJobID.Location = new System.Drawing.Point(43, 177);
            this.labelJobID.Name = "labelJobID";
            this.labelJobID.Size = new System.Drawing.Size(38, 13);
            this.labelJobID.TabIndex = 35;
            this.labelJobID.Text = "Job ID";
            // 
            // labelPayerID
            // 
            this.labelPayerID.AutoSize = true;
            this.labelPayerID.Location = new System.Drawing.Point(33, 150);
            this.labelPayerID.Name = "labelPayerID";
            this.labelPayerID.Size = new System.Drawing.Size(48, 13);
            this.labelPayerID.TabIndex = 34;
            this.labelPayerID.Text = "Payer ID";
            // 
            // labelPayeeID
            // 
            this.labelPayeeID.AutoSize = true;
            this.labelPayeeID.Location = new System.Drawing.Point(30, 123);
            this.labelPayeeID.Name = "labelPayeeID";
            this.labelPayeeID.Size = new System.Drawing.Size(51, 13);
            this.labelPayeeID.TabIndex = 33;
            this.labelPayeeID.Text = "Payee ID";
            // 
            // labelInvoiceDueDate
            // 
            this.labelInvoiceDueDate.AutoSize = true;
            this.labelInvoiceDueDate.Location = new System.Drawing.Point(13, 97);
            this.labelInvoiceDueDate.Name = "labelInvoiceDueDate";
            this.labelInvoiceDueDate.Size = new System.Drawing.Size(91, 13);
            this.labelInvoiceDueDate.TabIndex = 32;
            this.labelInvoiceDueDate.Text = "Invoice Due Date";
            // 
            // labelInvoiceDate
            // 
            this.labelInvoiceDate.AutoSize = true;
            this.labelInvoiceDate.Location = new System.Drawing.Point(13, 72);
            this.labelInvoiceDate.Name = "labelInvoiceDate";
            this.labelInvoiceDate.Size = new System.Drawing.Size(68, 13);
            this.labelInvoiceDate.TabIndex = 31;
            this.labelInvoiceDate.Text = "Invoice Date";
            // 
            // helpMenuStrip
            // 
            this.helpMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.helpMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.helpMenuStrip.Name = "helpMenuStrip";
            this.helpMenuStrip.Size = new System.Drawing.Size(800, 24);
            this.helpMenuStrip.TabIndex = 30;
            this.helpMenuStrip.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // labelInvoiceID
            // 
            this.labelInvoiceID.AutoSize = true;
            this.labelInvoiceID.Location = new System.Drawing.Point(13, 41);
            this.labelInvoiceID.Name = "labelInvoiceID";
            this.labelInvoiceID.Size = new System.Drawing.Size(56, 13);
            this.labelInvoiceID.TabIndex = 29;
            this.labelInvoiceID.Text = "Invoice ID";
            // 
            // buttonRefreshRecords
            // 
            this.buttonRefreshRecords.Location = new System.Drawing.Point(504, 209);
            this.buttonRefreshRecords.Name = "buttonRefreshRecords";
            this.buttonRefreshRecords.Size = new System.Drawing.Size(127, 23);
            this.buttonRefreshRecords.TabIndex = 54;
            this.buttonRefreshRecords.Text = "Refresh Records List";
            this.buttonRefreshRecords.UseVisualStyleBackColor = true;
            this.buttonRefreshRecords.Click += new System.EventHandler(this.buttonRefreshRecords_Click);
            // 
            // labelListOfInvoices
            // 
            this.labelListOfInvoices.AutoSize = true;
            this.labelListOfInvoices.Location = new System.Drawing.Point(314, 24);
            this.labelListOfInvoices.Name = "labelListOfInvoices";
            this.labelListOfInvoices.Size = new System.Drawing.Size(116, 13);
            this.labelListOfInvoices.TabIndex = 53;
            this.labelListOfInvoices.Text = "List of Invoice Records";
            // 
            // buttonEditInvoicePaymentStatus
            // 
            this.buttonEditInvoicePaymentStatus.Location = new System.Drawing.Point(655, 209);
            this.buttonEditInvoicePaymentStatus.Name = "buttonEditInvoicePaymentStatus";
            this.buttonEditInvoicePaymentStatus.Size = new System.Drawing.Size(133, 23);
            this.buttonEditInvoicePaymentStatus.TabIndex = 52;
            this.buttonEditInvoicePaymentStatus.Text = "Update Payment Status";
            this.buttonEditInvoicePaymentStatus.UseVisualStyleBackColor = true;
            this.buttonEditInvoicePaymentStatus.Click += new System.EventHandler(this.buttonEditInvoicePaymentStatus_Click);
            // 
            // dataGridViewListofInvoices
            // 
            this.dataGridViewListofInvoices.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridViewListofInvoices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListofInvoices.Location = new System.Drawing.Point(317, 41);
            this.dataGridViewListofInvoices.Name = "dataGridViewListofInvoices";
            this.dataGridViewListofInvoices.Size = new System.Drawing.Size(471, 162);
            this.dataGridViewListofInvoices.TabIndex = 55;
            this.dataGridViewListofInvoices.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewListofInvoices_CellContentClick);
            // 
            // labelClientName
            // 
            this.labelClientName.AutoSize = true;
            this.labelClientName.Location = new System.Drawing.Point(206, 151);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(101, 13);
            this.labelClientName.TabIndex = 56;
            this.labelClientName.Text = "(*Placeholder Text*)";
            // 
            // labelJobName
            // 
            this.labelJobName.AutoSize = true;
            this.labelJobName.Location = new System.Drawing.Point(210, 177);
            this.labelJobName.Name = "labelJobName";
            this.labelJobName.Size = new System.Drawing.Size(101, 13);
            this.labelJobName.TabIndex = 57;
            this.labelJobName.Text = "(*Placeholder Text*)";
            // 
            // labelFinalTotalValue
            // 
            this.labelFinalTotalValue.AutoSize = true;
            this.labelFinalTotalValue.Location = new System.Drawing.Point(108, 270);
            this.labelFinalTotalValue.Name = "labelFinalTotalValue";
            this.labelFinalTotalValue.Size = new System.Drawing.Size(122, 13);
            this.labelFinalTotalValue.TabIndex = 60;
            this.labelFinalTotalValue.Text = "*Awaiting Job Selection*";
            // 
            // labelVATValue
            // 
            this.labelVATValue.AutoSize = true;
            this.labelVATValue.Location = new System.Drawing.Point(108, 238);
            this.labelVATValue.Name = "labelVATValue";
            this.labelVATValue.Size = new System.Drawing.Size(122, 13);
            this.labelVATValue.TabIndex = 59;
            this.labelVATValue.Text = "*Awaiting Job Selection*";
            // 
            // labelSubtotalValue
            // 
            this.labelSubtotalValue.AutoSize = true;
            this.labelSubtotalValue.Location = new System.Drawing.Point(108, 209);
            this.labelSubtotalValue.Name = "labelSubtotalValue";
            this.labelSubtotalValue.Size = new System.Drawing.Size(122, 13);
            this.labelSubtotalValue.TabIndex = 58;
            this.labelSubtotalValue.Text = "*Awaiting Job Selection*";
            // 
            // frmViewInvoices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelFinalTotalValue);
            this.Controls.Add(this.labelVATValue);
            this.Controls.Add(this.labelSubtotalValue);
            this.Controls.Add(this.labelJobName);
            this.Controls.Add(this.labelClientName);
            this.Controls.Add(this.dataGridViewListofInvoices);
            this.Controls.Add(this.buttonRefreshRecords);
            this.Controls.Add(this.labelListOfInvoices);
            this.Controls.Add(this.buttonEditInvoicePaymentStatus);
            this.Controls.Add(this.textBoxConsoleLog);
            this.Controls.Add(this.labelMessageLog);
            this.Controls.Add(this.textBoxSubtotal);
            this.Controls.Add(this.textBoxVAT);
            this.Controls.Add(this.textBoxFinalTotal);
            this.Controls.Add(this.textBoxPaymentStatus);
            this.Controls.Add(this.textBoxJobIDValue);
            this.Controls.Add(this.textBoxPayerID);
            this.Controls.Add(this.textBoxPayeeIDValue);
            this.Controls.Add(this.dateTimePickerInvoiceDueDate);
            this.Controls.Add(this.dateTimePickerInvoiceDate);
            this.Controls.Add(this.labelInvoiceIDValue);
            this.Controls.Add(this.labelFinalTotal);
            this.Controls.Add(this.labelVAT);
            this.Controls.Add(this.labelPaymentStatus);
            this.Controls.Add(this.labelSubtotal);
            this.Controls.Add(this.labelJobID);
            this.Controls.Add(this.labelPayerID);
            this.Controls.Add(this.labelPayeeID);
            this.Controls.Add(this.labelInvoiceDueDate);
            this.Controls.Add(this.labelInvoiceDate);
            this.Controls.Add(this.helpMenuStrip);
            this.Controls.Add(this.labelInvoiceID);
            this.Name = "frmViewInvoices";
            this.Text = "View Outstanding Invoices";
            this.helpMenuStrip.ResumeLayout(false);
            this.helpMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListofInvoices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxConsoleLog;
        private System.Windows.Forms.Label labelMessageLog;
        private System.Windows.Forms.TextBox textBoxSubtotal;
        private System.Windows.Forms.TextBox textBoxVAT;
        private System.Windows.Forms.TextBox textBoxFinalTotal;
        private System.Windows.Forms.TextBox textBoxPaymentStatus;
        private System.Windows.Forms.TextBox textBoxJobIDValue;
        private System.Windows.Forms.TextBox textBoxPayerID;
        private System.Windows.Forms.TextBox textBoxPayeeIDValue;
        private System.Windows.Forms.DateTimePicker dateTimePickerInvoiceDueDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerInvoiceDate;
        private System.Windows.Forms.Label labelInvoiceIDValue;
        private System.Windows.Forms.Label labelFinalTotal;
        private System.Windows.Forms.Label labelVAT;
        private System.Windows.Forms.Label labelPaymentStatus;
        private System.Windows.Forms.Label labelSubtotal;
        private System.Windows.Forms.Label labelJobID;
        private System.Windows.Forms.Label labelPayerID;
        private System.Windows.Forms.Label labelPayeeID;
        private System.Windows.Forms.Label labelInvoiceDueDate;
        private System.Windows.Forms.Label labelInvoiceDate;
        private System.Windows.Forms.MenuStrip helpMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label labelInvoiceID;
        private System.Windows.Forms.Button buttonRefreshRecords;
        private System.Windows.Forms.Label labelListOfInvoices;
        private System.Windows.Forms.Button buttonEditInvoicePaymentStatus;
        private System.Windows.Forms.DataGridView dataGridViewListofInvoices;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.Label labelJobName;
        private System.Windows.Forms.Label labelFinalTotalValue;
        private System.Windows.Forms.Label labelVATValue;
        private System.Windows.Forms.Label labelSubtotalValue;
    }
}
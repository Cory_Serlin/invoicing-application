﻿
namespace Invoicing_Application
{
    partial class frmMainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.helpMenu = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creationInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editCompanyInfo = new System.Windows.Forms.Button();
            this.companyName = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.labelMainMenu = new System.Windows.Forms.Label();
            this.labelAtAGlanceTitle = new System.Windows.Forms.Label();
            this.labelNumberOfOutstandingInvoices = new System.Windows.Forms.Label();
            this.labelNumberOfOverdueInvoices = new System.Windows.Forms.Label();
            this.buttonRefreshStats = new System.Windows.Forms.Button();
            this.helpMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // helpMenu
            // 
            this.helpMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpMenu.Location = new System.Drawing.Point(0, 0);
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(452, 24);
            this.helpMenu.TabIndex = 0;
            this.helpMenu.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.creationInfoToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // creationInfoToolStripMenuItem
            // 
            this.creationInfoToolStripMenuItem.Name = "creationInfoToolStripMenuItem";
            this.creationInfoToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.creationInfoToolStripMenuItem.Text = "Creation Info";
            this.creationInfoToolStripMenuItem.Click += new System.EventHandler(this.aboutButtonOnClick);
            // 
            // editCompanyInfo
            // 
            this.editCompanyInfo.Location = new System.Drawing.Point(142, 194);
            this.editCompanyInfo.Name = "editCompanyInfo";
            this.editCompanyInfo.Size = new System.Drawing.Size(120, 23);
            this.editCompanyInfo.TabIndex = 1;
            this.editCompanyInfo.Text = "Edit Company Info";
            this.editCompanyInfo.UseVisualStyleBackColor = true;
            this.editCompanyInfo.Click += new System.EventHandler(this.navigationButtonClick);
            // 
            // companyName
            // 
            this.companyName.AutoSize = true;
            this.companyName.Location = new System.Drawing.Point(13, 36);
            this.companyName.Name = "companyName";
            this.companyName.Size = new System.Drawing.Size(173, 13);
            this.companyName.TabIndex = 2;
            this.companyName.Text = "Welcome to the Invoicing Software";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 194);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Enter Company Info";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.navigationButtonClick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(268, 194);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Create Job";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.navigationButtonClick);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(13, 223);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(119, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "View/Edit Jobs";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.navigationButtonClick);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(144, 223);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(118, 23);
            this.button4.TabIndex = 6;
            this.button4.Text = "Register New Client";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.navigationButtonClick);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(271, 223);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(117, 23);
            this.button5.TabIndex = 7;
            this.button5.Text = "View/Edit Clients";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.navigationButtonClick);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(16, 252);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(144, 23);
            this.button6.TabIndex = 8;
            this.button6.Text = "Create and Send Invoices";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.navigationButtonClick);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(166, 252);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(117, 23);
            this.button7.TabIndex = 9;
            this.button7.Text = "View Invoices";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.navigationButtonClick);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(289, 252);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(143, 23);
            this.button8.TabIndex = 10;
            this.button8.Text = "View/Generate Reports";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.navigationButtonClick);
            // 
            // labelMainMenu
            // 
            this.labelMainMenu.AutoSize = true;
            this.labelMainMenu.Location = new System.Drawing.Point(13, 178);
            this.labelMainMenu.Name = "labelMainMenu";
            this.labelMainMenu.Size = new System.Drawing.Size(60, 13);
            this.labelMainMenu.TabIndex = 11;
            this.labelMainMenu.Text = "Main Menu";
            // 
            // labelAtAGlanceTitle
            // 
            this.labelAtAGlanceTitle.AutoSize = true;
            this.labelAtAGlanceTitle.Location = new System.Drawing.Point(9, 79);
            this.labelAtAGlanceTitle.Name = "labelAtAGlanceTitle";
            this.labelAtAGlanceTitle.Size = new System.Drawing.Size(125, 13);
            this.labelAtAGlanceTitle.TabIndex = 12;
            this.labelAtAGlanceTitle.Text = "Invoice Stats at a glance";
            // 
            // labelNumberOfOutstandingInvoices
            // 
            this.labelNumberOfOutstandingInvoices.AutoSize = true;
            this.labelNumberOfOutstandingInvoices.Location = new System.Drawing.Point(20, 109);
            this.labelNumberOfOutstandingInvoices.Name = "labelNumberOfOutstandingInvoices";
            this.labelNumberOfOutstandingInvoices.Size = new System.Drawing.Size(95, 13);
            this.labelNumberOfOutstandingInvoices.TabIndex = 13;
            this.labelNumberOfOutstandingInvoices.Text = "*Placeholder Text*";
            // 
            // labelNumberOfOverdueInvoices
            // 
            this.labelNumberOfOverdueInvoices.AutoSize = true;
            this.labelNumberOfOverdueInvoices.Location = new System.Drawing.Point(20, 141);
            this.labelNumberOfOverdueInvoices.Name = "labelNumberOfOverdueInvoices";
            this.labelNumberOfOverdueInvoices.Size = new System.Drawing.Size(95, 13);
            this.labelNumberOfOverdueInvoices.TabIndex = 14;
            this.labelNumberOfOverdueInvoices.Text = "*Placeholder Text*";
            // 
            // buttonRefreshStats
            // 
            this.buttonRefreshStats.Location = new System.Drawing.Point(259, 120);
            this.buttonRefreshStats.Name = "buttonRefreshStats";
            this.buttonRefreshStats.Size = new System.Drawing.Size(129, 23);
            this.buttonRefreshStats.TabIndex = 15;
            this.buttonRefreshStats.Text = "Refresh Invoice Stats";
            this.buttonRefreshStats.UseVisualStyleBackColor = true;
            this.buttonRefreshStats.Click += new System.EventHandler(this.buttonRefreshStats_Click);
            // 
            // frmMainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 287);
            this.Controls.Add(this.buttonRefreshStats);
            this.Controls.Add(this.labelNumberOfOverdueInvoices);
            this.Controls.Add(this.labelNumberOfOutstandingInvoices);
            this.Controls.Add(this.labelAtAGlanceTitle);
            this.Controls.Add(this.labelMainMenu);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.companyName);
            this.Controls.Add(this.editCompanyInfo);
            this.Controls.Add(this.helpMenu);
            this.MainMenuStrip = this.helpMenu;
            this.Name = "frmMainScreen";
            this.Text = "Invoicing Application - Prototype";
            this.helpMenu.ResumeLayout(false);
            this.helpMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip helpMenu;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem creationInfoToolStripMenuItem;
        private System.Windows.Forms.Button editCompanyInfo;
        private System.Windows.Forms.Label companyName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label labelMainMenu;
        private System.Windows.Forms.Label labelAtAGlanceTitle;
        private System.Windows.Forms.Label labelNumberOfOutstandingInvoices;
        private System.Windows.Forms.Label labelNumberOfOverdueInvoices;
        private System.Windows.Forms.Button buttonRefreshStats;
    }
}


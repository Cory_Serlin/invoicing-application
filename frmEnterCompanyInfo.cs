﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Invoicing_Application
{
    /// <summary>
    /// Form code designed to handle the entering of information for a new company.
    /// </summary>
    public partial class frmEnterCompanyInfo : Form
    {
        //String value representing the table name 
        private String tableName = "Company";

        //String lists representing the columnNames and values
        private List<String> columnNames = new List<string>() {"Company_ID", "Company_Name","Company_Address","Company_Phone","Company_Email"};
        private List<String> columnValues;

        //Boolean Flags to identify which text box is empty 
        private Boolean companyNameEmpty = false;
        private Boolean companyAddressEmpty = false;
        private Boolean phoneNumberEmpty = false;
        private Boolean emailAddressEmpty = false;

        public frmEnterCompanyInfo()
        {
            int newID = 1;
            InitializeComponent();
            labelCompanyIDValue.Text = newID.ToString();
            columnValues = new List<string>();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Help button clicked");
            MessageBox.Show("Enter information about your company here. Press the submit company" +
                " button to submit your information!. Note you can only create one company however this information" +
                " can be edited later", "How to use this function");
        }

        private void submitCompanyInfoButtonOnClick(object sender, EventArgs e)
        {
            Console.WriteLine("Submit Company Info Button Clicked");
            checkTextBoxes();

            if(!companyNameEmpty && !companyAddressEmpty && !phoneNumberEmpty && !emailAddressEmpty)
            {
                Console.WriteLine("All Information present! Adding Company Information");
                textBoxConsoleLog.Text = "All Company Details Entered!";

                columnValues.Add(labelCompanyIDValue.Text);
                columnValues.Add(textBoxCompanyName.Text);
                columnValues.Add(textBoxCompanyAddress.Text);
                columnValues.Add(textBoxCompanyPhone.Text);
                columnValues.Add(textBoxCompanyEmail.Text);

                databaseOperationsManager.Current.addToTable(tableName, columnNames, columnValues);
                columnValues.Clear();
                Close();
            }

            else
            {
                Console.WriteLine("One or more text boxes are empty displaying on message log");
                textBoxConsoleLog.Text = setMessageLog(); 
            }

        }
        
        //Method designed to ensure all required text boxes have a value in it 
        public void checkTextBoxes()
        {
            companyNameEmpty = false;
            companyAddressEmpty = false;
            phoneNumberEmpty = false;
            emailAddressEmpty = false;
            
            if(String.IsNullOrEmpty(textBoxCompanyName.Text))
            {
                Console.WriteLine("No Company Name Entered");
                companyNameEmpty = true;
            }

            if(String.IsNullOrEmpty(textBoxCompanyAddress.Text))
            {
                Console.WriteLine("No Address Entered");
                companyAddressEmpty = true;
            }

            if(String.IsNullOrEmpty(textBoxCompanyPhone.Text))
            {
                Console.WriteLine("No Phone Number Entered");
                phoneNumberEmpty = true;
            }

            if(String.IsNullOrEmpty(textBoxCompanyEmail.Text))
            {
                Console.WriteLine("No Email Address Entered");
                emailAddressEmpty = true;
            }
        }

        //Method designed to display messages on the console log
        public string setMessageLog()
        {
            string messageLog = "";

            if(companyNameEmpty)
            {
                messageLog = messageLog + "No Company Name Entered! ";
            }

            if(companyAddressEmpty)
            {
                messageLog = messageLog + "No Address Entered! ";
            }

            if(phoneNumberEmpty)
            {
                messageLog = messageLog + "No Phone Number Entered! ";
            }

            if(emailAddressEmpty)
            {
                messageLog = messageLog + "No Email Address Entered! ";
            }

            return messageLog;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoicing_Application
{
    public partial class frmEditAccounts : Form
    {
        //String and String Lists to identify the table name, columns and values
        private string tableName = "Account";
        private List<String> columnNames = new List<string>() {"Account_Name", "Account_Phone", "Account_Email"};
        private List<String> columnValues;

        //Boolean Flags identifying potential empty text boxes
        private Boolean accountNameEmpty = false;
        private Boolean accountPhoneEmpty = false;
        private Boolean accountEmailEmpty = false;

        public frmEditAccounts()
        {
            InitializeComponent();
            dataGridViewListofAccounts.DataSource = databaseOperationsManager.Current.displayData(tableName);
            columnValues = new List<string>();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Help Button Clicked");
            MessageBox.Show("Select an account using the datagrid on the right.\n\n" +
                "Upon selection the account information will be inserted into the textboxes where you can edit" +
                "the information. Press update account record to submit changes.\n\n" +
                "Note: The system will automatically display the number of outstanding and overdue invoices that are associated" +
                "with this account", "How to use this function");
        }

        private void buttonUpdateAccountDetails_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Update Account Record Clicked");
            checkTextBoxes();

            if(!accountNameEmpty && !accountPhoneEmpty && !accountEmailEmpty)
            {
                Console.WriteLine("All Information Present! Checking for data to update");
                if(checkForDataToUpdate())
                {
                    Console.WriteLine("Records Availiable to update updating now");
                    updateData();
                    dataGridViewListofAccounts.DataSource = databaseOperationsManager.Current.displayData(tableName);
                }

                else
                {
                    textBoxConsoleLog.Text = "No Data to update";
                }
            }

            else
            {
                Console.WriteLine("One or more text boxes are empty displaying message on log");
                textBoxConsoleLog.Text = setMessageLog();
            }
        }

        private string setMessageLog()
        {
            string messageLog = "";

            if (accountNameEmpty)
            {
                messageLog = messageLog + "Account Name Empty";
            }

            if (accountPhoneEmpty)
            {
                messageLog = messageLog + "Account phone number empty";
            }

            if (accountEmailEmpty)
            {
                messageLog = messageLog + "Account email empty";
            }

            return messageLog;
        }

        private bool checkForDataToUpdate()
        {
            if(databaseOperationsManager.Current.checkDataColumn(tableName,"Account_Name",textBoxAccountName.Text,"Account_ID",labelAccountIDValue.Text))
            {
                return true;
            }

            else if (databaseOperationsManager.Current.checkDataColumn(tableName, "Account_Phone", textBoxAccountPhone.Text, "Account_ID", labelAccountIDValue.Text))
            {
                return true;
            }

            if (databaseOperationsManager.Current.checkDataColumn(tableName, "Account_Email", textBoxAccountEmail.Text, "Account_ID", labelAccountIDValue.Text))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        private void updateData()
        {
            columnValues.Add(textBoxAccountName.Text);
            columnValues.Add(textBoxAccountPhone.Text);
            columnValues.Add(textBoxAccountEmail.Text);
            databaseOperationsManager.Current.updateRow(tableName, "Account_ID", labelAccountIDValue.Text, columnNames, columnValues);
            columnValues.Clear();
        }

        private void checkTextBoxes()
        {
            accountNameEmpty = false;
            accountPhoneEmpty = false;
            accountEmailEmpty = false;

            if (String.IsNullOrEmpty(textBoxAccountName.Text))
            {
                Console.WriteLine("Account Name not written");
                accountNameEmpty = true;
            }

            if (String.IsNullOrEmpty(textBoxAccountPhone.Text))
            {
                Console.WriteLine("Account Phone not written");
                accountPhoneEmpty = true;
            }

            if (String.IsNullOrEmpty(textBoxAccountEmail.Text))
            {
                Console.WriteLine("Account Email not written");
                accountEmailEmpty = true;
            }
        }

        private void buttonRefreshRecords_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Refresh Records Button clicked!");
            dataGridViewListofAccounts.DataSource = databaseOperationsManager.Current.displayData(tableName);
        }

        private void dataGridViewListofAccounts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Console.WriteLine("Cell clicked at row " + dataGridViewListofAccounts.CurrentCell.RowIndex);
            int index = dataGridViewListofAccounts.CurrentCell.RowIndex;
            DataGridViewRow selectedRow = dataGridViewListofAccounts.Rows[index];

            labelAccountIDValue.Text = selectedRow.Cells[0].Value.ToString();
            textBoxAccountName.Text = selectedRow.Cells[1].Value.ToString();
            textBoxAccountPhone.Text = selectedRow.Cells[2].Value.ToString();
            textBoxAccountEmail.Text = selectedRow.Cells[3].Value.ToString();
            textBoxOutstandingInvoices.Text = databaseOperationsManager.Current.reportGeneration("SELECT COUNT(*) FROM Invoice WHERE Payer_ID = " + labelAccountIDValue.Text + " and Payment_Status = 'U'");
            textBoxOverdueInvoices.Text = databaseOperationsManager.Current.reportGeneration("SELECT COUNT(*) FROM Invoice WHERE Payer_ID = " + labelAccountIDValue.Text + " and Payment_Status = 'U' and CAST(Invoice_Due_Date AS Date) <= CAST(GetDate() AS Date)");
        }
    }
}

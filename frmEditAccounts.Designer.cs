﻿
namespace Invoicing_Application
{
    partial class frmEditAccounts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.helpMenuStrip = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonUpdateAccountDetails = new System.Windows.Forms.Button();
            this.textBoxConsoleLog = new System.Windows.Forms.TextBox();
            this.labelMessageLog = new System.Windows.Forms.Label();
            this.labelOutstandingInvoicesCount = new System.Windows.Forms.Label();
            this.labelAccountEmail = new System.Windows.Forms.Label();
            this.textBoxAccountEmail = new System.Windows.Forms.TextBox();
            this.textBoxOutstandingInvoices = new System.Windows.Forms.TextBox();
            this.textBoxAccountPhone = new System.Windows.Forms.TextBox();
            this.labelAccountPhone = new System.Windows.Forms.Label();
            this.textBoxAccountName = new System.Windows.Forms.TextBox();
            this.labelAccountName = new System.Windows.Forms.Label();
            this.labelAccountIDValue = new System.Windows.Forms.Label();
            this.labelAccountID = new System.Windows.Forms.Label();
            this.buttonRefreshRecords = new System.Windows.Forms.Button();
            this.labelListOfAccounts = new System.Windows.Forms.Label();
            this.dataGridViewListofAccounts = new System.Windows.Forms.DataGridView();
            this.labelCountOverdueInvoices = new System.Windows.Forms.Label();
            this.textBoxOverdueInvoices = new System.Windows.Forms.TextBox();
            this.helpMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListofAccounts)).BeginInit();
            this.SuspendLayout();
            // 
            // helpMenuStrip
            // 
            this.helpMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.helpMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.helpMenuStrip.Name = "helpMenuStrip";
            this.helpMenuStrip.Size = new System.Drawing.Size(800, 24);
            this.helpMenuStrip.TabIndex = 3;
            this.helpMenuStrip.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // buttonUpdateAccountDetails
            // 
            this.buttonUpdateAccountDetails.Location = new System.Drawing.Point(648, 415);
            this.buttonUpdateAccountDetails.Name = "buttonUpdateAccountDetails";
            this.buttonUpdateAccountDetails.Size = new System.Drawing.Size(140, 23);
            this.buttonUpdateAccountDetails.TabIndex = 31;
            this.buttonUpdateAccountDetails.Text = "Update Account Record";
            this.buttonUpdateAccountDetails.UseVisualStyleBackColor = true;
            this.buttonUpdateAccountDetails.Click += new System.EventHandler(this.buttonUpdateAccountDetails_Click);
            // 
            // textBoxConsoleLog
            // 
            this.textBoxConsoleLog.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxConsoleLog.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxConsoleLog.Location = new System.Drawing.Point(21, 223);
            this.textBoxConsoleLog.Multiline = true;
            this.textBoxConsoleLog.Name = "textBoxConsoleLog";
            this.textBoxConsoleLog.ReadOnly = true;
            this.textBoxConsoleLog.Size = new System.Drawing.Size(369, 176);
            this.textBoxConsoleLog.TabIndex = 30;
            // 
            // labelMessageLog
            // 
            this.labelMessageLog.AutoSize = true;
            this.labelMessageLog.Location = new System.Drawing.Point(22, 207);
            this.labelMessageLog.Name = "labelMessageLog";
            this.labelMessageLog.Size = new System.Drawing.Size(71, 13);
            this.labelMessageLog.TabIndex = 29;
            this.labelMessageLog.Text = "Message Log";
            // 
            // labelOutstandingInvoicesCount
            // 
            this.labelOutstandingInvoicesCount.AutoSize = true;
            this.labelOutstandingInvoicesCount.Location = new System.Drawing.Point(18, 150);
            this.labelOutstandingInvoicesCount.Name = "labelOutstandingInvoicesCount";
            this.labelOutstandingInvoicesCount.Size = new System.Drawing.Size(141, 13);
            this.labelOutstandingInvoicesCount.TabIndex = 28;
            this.labelOutstandingInvoicesCount.Text = "No. Of Outstanding Invoices";
            // 
            // labelAccountEmail
            // 
            this.labelAccountEmail.AutoSize = true;
            this.labelAccountEmail.Location = new System.Drawing.Point(18, 117);
            this.labelAccountEmail.Name = "labelAccountEmail";
            this.labelAccountEmail.Size = new System.Drawing.Size(75, 13);
            this.labelAccountEmail.TabIndex = 27;
            this.labelAccountEmail.Text = "Account Email";
            // 
            // textBoxAccountEmail
            // 
            this.textBoxAccountEmail.Location = new System.Drawing.Point(96, 117);
            this.textBoxAccountEmail.Name = "textBoxAccountEmail";
            this.textBoxAccountEmail.Size = new System.Drawing.Size(294, 20);
            this.textBoxAccountEmail.TabIndex = 26;
            // 
            // textBoxOutstandingInvoices
            // 
            this.textBoxOutstandingInvoices.Location = new System.Drawing.Point(165, 147);
            this.textBoxOutstandingInvoices.Name = "textBoxOutstandingInvoices";
            this.textBoxOutstandingInvoices.ReadOnly = true;
            this.textBoxOutstandingInvoices.Size = new System.Drawing.Size(225, 20);
            this.textBoxOutstandingInvoices.TabIndex = 25;
            // 
            // textBoxAccountPhone
            // 
            this.textBoxAccountPhone.Location = new System.Drawing.Point(99, 84);
            this.textBoxAccountPhone.Name = "textBoxAccountPhone";
            this.textBoxAccountPhone.Size = new System.Drawing.Size(291, 20);
            this.textBoxAccountPhone.TabIndex = 24;
            // 
            // labelAccountPhone
            // 
            this.labelAccountPhone.AutoSize = true;
            this.labelAccountPhone.Location = new System.Drawing.Point(12, 89);
            this.labelAccountPhone.Name = "labelAccountPhone";
            this.labelAccountPhone.Size = new System.Drawing.Size(81, 13);
            this.labelAccountPhone.TabIndex = 23;
            this.labelAccountPhone.Text = "Account Phone";
            // 
            // textBoxAccountName
            // 
            this.textBoxAccountName.Location = new System.Drawing.Point(96, 52);
            this.textBoxAccountName.Name = "textBoxAccountName";
            this.textBoxAccountName.Size = new System.Drawing.Size(291, 20);
            this.textBoxAccountName.TabIndex = 22;
            // 
            // labelAccountName
            // 
            this.labelAccountName.AutoSize = true;
            this.labelAccountName.Location = new System.Drawing.Point(12, 55);
            this.labelAccountName.Name = "labelAccountName";
            this.labelAccountName.Size = new System.Drawing.Size(78, 13);
            this.labelAccountName.TabIndex = 21;
            this.labelAccountName.Text = "Account Name";
            // 
            // labelAccountIDValue
            // 
            this.labelAccountIDValue.AutoSize = true;
            this.labelAccountIDValue.Location = new System.Drawing.Point(79, 24);
            this.labelAccountIDValue.Name = "labelAccountIDValue";
            this.labelAccountIDValue.Size = new System.Drawing.Size(162, 13);
            this.labelAccountIDValue.TabIndex = 20;
            this.labelAccountIDValue.Text = "*N/A (Select a row on the table)*";
            // 
            // labelAccountID
            // 
            this.labelAccountID.AutoSize = true;
            this.labelAccountID.Location = new System.Drawing.Point(12, 24);
            this.labelAccountID.Name = "labelAccountID";
            this.labelAccountID.Size = new System.Drawing.Size(61, 13);
            this.labelAccountID.TabIndex = 19;
            this.labelAccountID.Text = "Account ID";
            // 
            // buttonRefreshRecords
            // 
            this.buttonRefreshRecords.Location = new System.Drawing.Point(515, 415);
            this.buttonRefreshRecords.Name = "buttonRefreshRecords";
            this.buttonRefreshRecords.Size = new System.Drawing.Size(117, 23);
            this.buttonRefreshRecords.TabIndex = 33;
            this.buttonRefreshRecords.Text = "Refresh Records List";
            this.buttonRefreshRecords.UseVisualStyleBackColor = true;
            this.buttonRefreshRecords.Click += new System.EventHandler(this.buttonRefreshRecords_Click);
            // 
            // labelListOfAccounts
            // 
            this.labelListOfAccounts.AutoSize = true;
            this.labelListOfAccounts.Location = new System.Drawing.Point(415, 36);
            this.labelListOfAccounts.Name = "labelListOfAccounts";
            this.labelListOfAccounts.Size = new System.Drawing.Size(83, 13);
            this.labelListOfAccounts.TabIndex = 32;
            this.labelListOfAccounts.Text = "List of Accounts";
            // 
            // dataGridViewListofAccounts
            // 
            this.dataGridViewListofAccounts.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridViewListofAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListofAccounts.Location = new System.Drawing.Point(418, 57);
            this.dataGridViewListofAccounts.Name = "dataGridViewListofAccounts";
            this.dataGridViewListofAccounts.Size = new System.Drawing.Size(370, 342);
            this.dataGridViewListofAccounts.TabIndex = 34;
            this.dataGridViewListofAccounts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewListofAccounts_CellClick);
            // 
            // labelCountOverdueInvoices
            // 
            this.labelCountOverdueInvoices.AutoSize = true;
            this.labelCountOverdueInvoices.Location = new System.Drawing.Point(34, 185);
            this.labelCountOverdueInvoices.Name = "labelCountOverdueInvoices";
            this.labelCountOverdueInvoices.Size = new System.Drawing.Size(125, 13);
            this.labelCountOverdueInvoices.TabIndex = 35;
            this.labelCountOverdueInvoices.Text = "No. Of Overdue Invoices";
            // 
            // textBoxOverdueInvoices
            // 
            this.textBoxOverdueInvoices.Location = new System.Drawing.Point(165, 185);
            this.textBoxOverdueInvoices.Name = "textBoxOverdueInvoices";
            this.textBoxOverdueInvoices.ReadOnly = true;
            this.textBoxOverdueInvoices.Size = new System.Drawing.Size(222, 20);
            this.textBoxOverdueInvoices.TabIndex = 36;
            // 
            // frmEditAccounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBoxOverdueInvoices);
            this.Controls.Add(this.labelCountOverdueInvoices);
            this.Controls.Add(this.dataGridViewListofAccounts);
            this.Controls.Add(this.buttonRefreshRecords);
            this.Controls.Add(this.labelListOfAccounts);
            this.Controls.Add(this.buttonUpdateAccountDetails);
            this.Controls.Add(this.textBoxConsoleLog);
            this.Controls.Add(this.labelMessageLog);
            this.Controls.Add(this.labelOutstandingInvoicesCount);
            this.Controls.Add(this.labelAccountEmail);
            this.Controls.Add(this.textBoxAccountEmail);
            this.Controls.Add(this.textBoxOutstandingInvoices);
            this.Controls.Add(this.textBoxAccountPhone);
            this.Controls.Add(this.labelAccountPhone);
            this.Controls.Add(this.textBoxAccountName);
            this.Controls.Add(this.labelAccountName);
            this.Controls.Add(this.labelAccountIDValue);
            this.Controls.Add(this.labelAccountID);
            this.Controls.Add(this.helpMenuStrip);
            this.Name = "frmEditAccounts";
            this.Text = "Edit Client Info";
            this.helpMenuStrip.ResumeLayout(false);
            this.helpMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListofAccounts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip helpMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Button buttonUpdateAccountDetails;
        private System.Windows.Forms.TextBox textBoxConsoleLog;
        private System.Windows.Forms.Label labelMessageLog;
        private System.Windows.Forms.Label labelOutstandingInvoicesCount;
        private System.Windows.Forms.Label labelAccountEmail;
        private System.Windows.Forms.TextBox textBoxAccountEmail;
        private System.Windows.Forms.TextBox textBoxOutstandingInvoices;
        private System.Windows.Forms.TextBox textBoxAccountPhone;
        private System.Windows.Forms.Label labelAccountPhone;
        private System.Windows.Forms.TextBox textBoxAccountName;
        private System.Windows.Forms.Label labelAccountName;
        private System.Windows.Forms.Label labelAccountIDValue;
        private System.Windows.Forms.Label labelAccountID;
        private System.Windows.Forms.Button buttonRefreshRecords;
        private System.Windows.Forms.Label labelListOfAccounts;
        private System.Windows.Forms.DataGridView dataGridViewListofAccounts;
        private System.Windows.Forms.Label labelCountOverdueInvoices;
        private System.Windows.Forms.TextBox textBoxOverdueInvoices;
    }
}
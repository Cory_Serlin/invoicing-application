﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoicing_Application
{
    public partial class frmReportGeneration : Form
    {
        //Private list for list of jobs
        private List<String> listOfJobNames; 
        
        public frmReportGeneration()
        {
            InitializeComponent();
            listOfJobNames = new List<string>();
            listOfJobNames = databaseOperationsManager.Current.getColumnValues(listOfJobNames, "Job_Name", "Job");
            FillComboBox(listOfJobNames, comboBoxListOfJobs);            
        }

        private void comboBoxReportOptionsValueChanged(object sender, EventArgs e)
        {
            Console.WriteLine("Combo changed to value: " + comboBoxReportOptions.Text);

            switch (comboBoxReportOptions.Text.ToLower())
            {
                case "financial":
                    Console.WriteLine("Generating Financial Report");
                    financialReportGeneration();
                    break;

                case "statistical":
                    Console.WriteLine("Generating Statistical Report");
                    statisticalReportGeneration();
                    break;
            }
        }

        private void statisticalReportGeneration()
        {
            String totalNumberOfInvoices = "Total Number of invoices: " + databaseOperationsManager.Current.reportGeneration("SELECT COUNT(*) FROM Invoice");
            String totalNumberOfUnpaidInvoices = "Total Invoices Unpaid " + databaseOperationsManager.Current.reportGeneration("SELECT COUNT(*) FROM Invoice WHERE Payment_Status = 'U'");
            textBoxReportLog.Text = totalNumberOfInvoices + "\n" + totalNumberOfUnpaidInvoices;
        }

        private void financialReportGeneration()
        {
            String totalIncome = "Total Income = £" + databaseOperationsManager.Current.reportGeneration("SELECT SUM(Subtotal) FROM Invoice") + "\n";
            String totalVAT = " Total VAT = £" + databaseOperationsManager.Current.reportGeneration("SELECT SUM(VAT) FROM Invoice");
            textBoxReportLog.Text = totalIncome + "\n" + totalVAT;
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Help button pressed");
            MessageBox.Show("Select a type of report to show using the drop down box below.\n" +
                "When a report type is selected, a report based on the type will be displayed", "How to use this function");
        }

        //Code to work out the logic for getting job names to the combo box
        public void FillComboBox(List<String> values, ComboBox comboBoxToFill)
        {
            for (int i = 0; i < values.Count; i++)
            {
                comboBoxToFill.Items.Add(values[i]);
            }
        }

        private void comboBoxListOfJobNamesIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("Job Name Index Changed");
            labelJobIDValue.Text = databaseOperationsManager.Current.selectRow("Job", "Job_ID", "Job_Name", comboBoxListOfJobs.Text);
            labelJobStatsReport.Text = "This job has been requested: " + databaseOperationsManager.Current.reportGeneration("SELECT COUNT(*) FROM Invoice WHERE Job_ID = " + labelJobIDValue.Text) + " Times";

        }
    }
}

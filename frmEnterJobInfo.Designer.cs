﻿
namespace Invoicing_Application
{
    partial class frmEnterJobInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.helpMenuStrip = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelJobID = new System.Windows.Forms.Label();
            this.labelJobIDValue = new System.Windows.Forms.Label();
            this.labelJobName = new System.Windows.Forms.Label();
            this.textBoxJobName = new System.Windows.Forms.TextBox();
            this.labelJobCost = new System.Windows.Forms.Label();
            this.textBoxJobCost = new System.Windows.Forms.TextBox();
            this.labelMessageLog = new System.Windows.Forms.Label();
            this.textBoxConsoleLog = new System.Windows.Forms.TextBox();
            this.buttonCreateNewJob = new System.Windows.Forms.Button();
            this.labelJobCostasCurrency = new System.Windows.Forms.Label();
            this.helpMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // helpMenuStrip
            // 
            this.helpMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.helpMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.helpMenuStrip.Name = "helpMenuStrip";
            this.helpMenuStrip.Size = new System.Drawing.Size(321, 24);
            this.helpMenuStrip.TabIndex = 1;
            this.helpMenuStrip.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // labelJobID
            // 
            this.labelJobID.AutoSize = true;
            this.labelJobID.Location = new System.Drawing.Point(13, 28);
            this.labelJobID.Name = "labelJobID";
            this.labelJobID.Size = new System.Drawing.Size(38, 13);
            this.labelJobID.TabIndex = 2;
            this.labelJobID.Text = "Job ID";
            // 
            // labelJobIDValue
            // 
            this.labelJobIDValue.AutoSize = true;
            this.labelJobIDValue.Location = new System.Drawing.Point(58, 28);
            this.labelJobIDValue.Name = "labelJobIDValue";
            this.labelJobIDValue.Size = new System.Drawing.Size(95, 13);
            this.labelJobIDValue.TabIndex = 3;
            this.labelJobIDValue.Text = "*Placeholder Text*";
            // 
            // labelJobName
            // 
            this.labelJobName.AutoSize = true;
            this.labelJobName.Location = new System.Drawing.Point(0, 66);
            this.labelJobName.Name = "labelJobName";
            this.labelJobName.Size = new System.Drawing.Size(55, 13);
            this.labelJobName.TabIndex = 4;
            this.labelJobName.Text = "Job Name";
            // 
            // textBoxJobName
            // 
            this.textBoxJobName.Location = new System.Drawing.Point(61, 63);
            this.textBoxJobName.Name = "textBoxJobName";
            this.textBoxJobName.Size = new System.Drawing.Size(243, 20);
            this.textBoxJobName.TabIndex = 5;
            // 
            // labelJobCost
            // 
            this.labelJobCost.AutoSize = true;
            this.labelJobCost.Location = new System.Drawing.Point(3, 121);
            this.labelJobCost.Name = "labelJobCost";
            this.labelJobCost.Size = new System.Drawing.Size(48, 13);
            this.labelJobCost.TabIndex = 8;
            this.labelJobCost.Text = "Job Cost";
            // 
            // textBoxJobCost
            // 
            this.textBoxJobCost.Location = new System.Drawing.Point(61, 121);
            this.textBoxJobCost.Name = "textBoxJobCost";
            this.textBoxJobCost.Size = new System.Drawing.Size(134, 20);
            this.textBoxJobCost.TabIndex = 9;
            this.textBoxJobCost.TextChanged += new System.EventHandler(this.textBoxJobCostChanged);
            // 
            // labelMessageLog
            // 
            this.labelMessageLog.AutoSize = true;
            this.labelMessageLog.Location = new System.Drawing.Point(3, 217);
            this.labelMessageLog.Name = "labelMessageLog";
            this.labelMessageLog.Size = new System.Drawing.Size(71, 13);
            this.labelMessageLog.TabIndex = 10;
            this.labelMessageLog.Text = "Message Log";
            // 
            // textBoxConsoleLog
            // 
            this.textBoxConsoleLog.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxConsoleLog.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxConsoleLog.Location = new System.Drawing.Point(6, 233);
            this.textBoxConsoleLog.Multiline = true;
            this.textBoxConsoleLog.Name = "textBoxConsoleLog";
            this.textBoxConsoleLog.ReadOnly = true;
            this.textBoxConsoleLog.Size = new System.Drawing.Size(298, 176);
            this.textBoxConsoleLog.TabIndex = 16;
            // 
            // buttonCreateNewJob
            // 
            this.buttonCreateNewJob.Location = new System.Drawing.Point(208, 415);
            this.buttonCreateNewJob.Name = "buttonCreateNewJob";
            this.buttonCreateNewJob.Size = new System.Drawing.Size(95, 23);
            this.buttonCreateNewJob.TabIndex = 17;
            this.buttonCreateNewJob.Text = "Create New Job";
            this.buttonCreateNewJob.UseVisualStyleBackColor = true;
            this.buttonCreateNewJob.Click += new System.EventHandler(this.buttonCreateNewJob_Click);
            // 
            // labelJobCostasCurrency
            // 
            this.labelJobCostasCurrency.AutoSize = true;
            this.labelJobCostasCurrency.Location = new System.Drawing.Point(201, 124);
            this.labelJobCostasCurrency.Name = "labelJobCostasCurrency";
            this.labelJobCostasCurrency.Size = new System.Drawing.Size(115, 13);
            this.labelJobCostasCurrency.TabIndex = 23;
            this.labelJobCostasCurrency.Text = "(Awaiting Input of cost)";
            // 
            // frmEnterJobInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 450);
            this.Controls.Add(this.labelJobCostasCurrency);
            this.Controls.Add(this.buttonCreateNewJob);
            this.Controls.Add(this.textBoxConsoleLog);
            this.Controls.Add(this.labelMessageLog);
            this.Controls.Add(this.textBoxJobCost);
            this.Controls.Add(this.labelJobCost);
            this.Controls.Add(this.textBoxJobName);
            this.Controls.Add(this.labelJobName);
            this.Controls.Add(this.labelJobIDValue);
            this.Controls.Add(this.labelJobID);
            this.Controls.Add(this.helpMenuStrip);
            this.Name = "frmEnterJobInfo";
            this.Text = "Enter New Job";
            this.helpMenuStrip.ResumeLayout(false);
            this.helpMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip helpMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label labelJobID;
        private System.Windows.Forms.Label labelJobIDValue;
        private System.Windows.Forms.Label labelJobName;
        private System.Windows.Forms.TextBox textBoxJobName;
        private System.Windows.Forms.Label labelJobCost;
        private System.Windows.Forms.TextBox textBoxJobCost;
        private System.Windows.Forms.Label labelMessageLog;
        private System.Windows.Forms.TextBox textBoxConsoleLog;
        private System.Windows.Forms.Button buttonCreateNewJob;
        private System.Windows.Forms.Label labelJobCostasCurrency;
    }
}
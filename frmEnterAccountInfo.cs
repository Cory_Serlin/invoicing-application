﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoicing_Application
{
    public partial class frmEnterAccountInfo : Form
    {
        //Int value representing the ID of the new Account
        private int ID = 0;

        //String and String Lists representing the table name, column names and column values
        private String tableName = "Account";
        private List<String> columnNames = new List<string>() { "Account_ID", "Account_Name", "Account_Phone", "Account_Email"};
        private List<String> columnValues;

        //Boolean Flags identifying potential empty text boxes
        private Boolean accountNameEmpty = false;
        private Boolean accountPhoneEmpty = false;
        private Boolean accountEmailEmpty = false;

        public frmEnterAccountInfo()
        {
            InitializeComponent();
            ID = databaseOperationsManager.Current.IDGenerator();
            labelAccountIDValue.Text = ID.ToString();
            columnValues = new List<string>();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Help Button Pressed");
            MessageBox.Show("Enter the account information into the text boxes and press" +
                " Create New Account to add the account to the application", "How to use this function");
        }

        private void buttonCreateNewAccount_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Create New Account Pressed");
            textBoxConsoleLog.Text = "";
            checkTextBoxes();

            if(!accountNameEmpty && !accountPhoneEmpty && !accountEmailEmpty)
            {
                Console.WriteLine("All Data Entered! Adding new Account");
                textBoxConsoleLog.Text = "All Account Values Entered";

                columnValues.Add(labelAccountIDValue.Text);
                columnValues.Add(textBoxAccountName.Text);
                columnValues.Add(textBoxAccountPhone.Text);
                columnValues.Add(textBoxAccountEmail.Text);

                databaseOperationsManager.Current.addToTable(tableName, columnNames, columnValues);
                columnValues.Clear();

                ID = databaseOperationsManager.Current.IDGenerator();
                labelAccountIDValue.Text = ID.ToString();
                textBoxAccountName.Text = "";
                textBoxAccountPhone.Text = "";
                textBoxAccountEmail.Text = "";
            }

            else
            {
                Console.WriteLine("One or more text boxes are empty displaying on message log");
                textBoxConsoleLog.Text = setMessageLog();
            }
        }

        private string setMessageLog()
        {
            string messageLog = "";

            if(accountNameEmpty)
            {
                messageLog = messageLog + "Account Name Empty";
            }

            if(accountPhoneEmpty)
            {
                messageLog = messageLog + "Account phone number empty";
            }

            if(accountEmailEmpty)
            {
                messageLog = messageLog + "Account email empty";
            }

            return messageLog;
        }

        private void checkTextBoxes()
        {
            accountNameEmpty = false;
            accountPhoneEmpty = false;
            accountEmailEmpty = false;
                       
            if(String.IsNullOrEmpty(textBoxAccountName.Text))
            {
                Console.WriteLine("Account Name not written");
                accountNameEmpty = true;
            }

            if(String.IsNullOrEmpty(textBoxAccountPhone.Text))
            {
                Console.WriteLine("Account Phone not written");
                accountPhoneEmpty = true;
            }

            if(String.IsNullOrEmpty(textBoxAccountEmail.Text))
            {
                Console.WriteLine("Account Email not written");
                accountEmailEmpty = true; 
            }
        }
    }
}

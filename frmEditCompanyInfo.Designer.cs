﻿
namespace Invoicing_Application
{
    partial class frmEditCompanyInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.helpMenuStrip = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelCompanyID = new System.Windows.Forms.Label();
            this.textBoxCompanyPhone = new System.Windows.Forms.TextBox();
            this.textBoxCompanyEmail = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelCompanyPhone = new System.Windows.Forms.Label();
            this.labelCompanyAddress = new System.Windows.Forms.Label();
            this.textBoxCompanyAddress = new System.Windows.Forms.TextBox();
            this.textBoxCompanyName = new System.Windows.Forms.TextBox();
            this.labelCompanyName = new System.Windows.Forms.Label();
            this.labelCompanyIDValue = new System.Windows.Forms.Label();
            this.buttonUpdateCompanyInfo = new System.Windows.Forms.Button();
            this.textBoxConsoleLog = new System.Windows.Forms.TextBox();
            this.labelMessageLog = new System.Windows.Forms.Label();
            this.helpMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // helpMenuStrip
            // 
            this.helpMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.helpMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.helpMenuStrip.Name = "helpMenuStrip";
            this.helpMenuStrip.Size = new System.Drawing.Size(384, 24);
            this.helpMenuStrip.TabIndex = 0;
            this.helpMenuStrip.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // labelCompanyID
            // 
            this.labelCompanyID.AutoSize = true;
            this.labelCompanyID.Location = new System.Drawing.Point(24, 44);
            this.labelCompanyID.Name = "labelCompanyID";
            this.labelCompanyID.Size = new System.Drawing.Size(65, 13);
            this.labelCompanyID.TabIndex = 2;
            this.labelCompanyID.Text = "Company ID";
            // 
            // textBoxCompanyPhone
            // 
            this.textBoxCompanyPhone.Location = new System.Drawing.Point(118, 159);
            this.textBoxCompanyPhone.Name = "textBoxCompanyPhone";
            this.textBoxCompanyPhone.Size = new System.Drawing.Size(254, 20);
            this.textBoxCompanyPhone.TabIndex = 21;
            // 
            // textBoxCompanyEmail
            // 
            this.textBoxCompanyEmail.Location = new System.Drawing.Point(118, 205);
            this.textBoxCompanyEmail.Name = "textBoxCompanyEmail";
            this.textBoxCompanyEmail.Size = new System.Drawing.Size(254, 20);
            this.textBoxCompanyEmail.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Email Address";
            // 
            // labelCompanyPhone
            // 
            this.labelCompanyPhone.AutoSize = true;
            this.labelCompanyPhone.Location = new System.Drawing.Point(16, 166);
            this.labelCompanyPhone.Name = "labelCompanyPhone";
            this.labelCompanyPhone.Size = new System.Drawing.Size(78, 13);
            this.labelCompanyPhone.TabIndex = 18;
            this.labelCompanyPhone.Text = "Phone Number";
            // 
            // labelCompanyAddress
            // 
            this.labelCompanyAddress.AutoSize = true;
            this.labelCompanyAddress.Location = new System.Drawing.Point(2, 124);
            this.labelCompanyAddress.Name = "labelCompanyAddress";
            this.labelCompanyAddress.Size = new System.Drawing.Size(92, 13);
            this.labelCompanyAddress.TabIndex = 17;
            this.labelCompanyAddress.Text = "Company Address";
            // 
            // textBoxCompanyAddress
            // 
            this.textBoxCompanyAddress.Location = new System.Drawing.Point(118, 117);
            this.textBoxCompanyAddress.Name = "textBoxCompanyAddress";
            this.textBoxCompanyAddress.Size = new System.Drawing.Size(254, 20);
            this.textBoxCompanyAddress.TabIndex = 16;
            // 
            // textBoxCompanyName
            // 
            this.textBoxCompanyName.Location = new System.Drawing.Point(118, 71);
            this.textBoxCompanyName.Name = "textBoxCompanyName";
            this.textBoxCompanyName.Size = new System.Drawing.Size(254, 20);
            this.textBoxCompanyName.TabIndex = 15;
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.AutoSize = true;
            this.labelCompanyName.Location = new System.Drawing.Point(12, 78);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Size = new System.Drawing.Size(82, 13);
            this.labelCompanyName.TabIndex = 14;
            this.labelCompanyName.Text = "Company Name";
            // 
            // labelCompanyIDValue
            // 
            this.labelCompanyIDValue.AutoSize = true;
            this.labelCompanyIDValue.Location = new System.Drawing.Point(115, 44);
            this.labelCompanyIDValue.Name = "labelCompanyIDValue";
            this.labelCompanyIDValue.Size = new System.Drawing.Size(101, 13);
            this.labelCompanyIDValue.TabIndex = 13;
            this.labelCompanyIDValue.Text = "*Placeholder Value*";
            // 
            // buttonUpdateCompanyInfo
            // 
            this.buttonUpdateCompanyInfo.Location = new System.Drawing.Point(250, 352);
            this.buttonUpdateCompanyInfo.Name = "buttonUpdateCompanyInfo";
            this.buttonUpdateCompanyInfo.Size = new System.Drawing.Size(122, 23);
            this.buttonUpdateCompanyInfo.TabIndex = 22;
            this.buttonUpdateCompanyInfo.Text = "Update Company Info";
            this.buttonUpdateCompanyInfo.UseVisualStyleBackColor = true;
            this.buttonUpdateCompanyInfo.Click += new System.EventHandler(this.buttonUpdateCompanyInfo_Click);
            // 
            // textBoxConsoleLog
            // 
            this.textBoxConsoleLog.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxConsoleLog.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxConsoleLog.Location = new System.Drawing.Point(19, 272);
            this.textBoxConsoleLog.Multiline = true;
            this.textBoxConsoleLog.Name = "textBoxConsoleLog";
            this.textBoxConsoleLog.ReadOnly = true;
            this.textBoxConsoleLog.Size = new System.Drawing.Size(193, 103);
            this.textBoxConsoleLog.TabIndex = 23;
            // 
            // labelMessageLog
            // 
            this.labelMessageLog.AutoSize = true;
            this.labelMessageLog.Location = new System.Drawing.Point(18, 256);
            this.labelMessageLog.Name = "labelMessageLog";
            this.labelMessageLog.Size = new System.Drawing.Size(71, 13);
            this.labelMessageLog.TabIndex = 24;
            this.labelMessageLog.Text = "Message Log";
            // 
            // frmEditCompanyInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 387);
            this.Controls.Add(this.labelMessageLog);
            this.Controls.Add(this.textBoxConsoleLog);
            this.Controls.Add(this.buttonUpdateCompanyInfo);
            this.Controls.Add(this.textBoxCompanyPhone);
            this.Controls.Add(this.textBoxCompanyEmail);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelCompanyPhone);
            this.Controls.Add(this.labelCompanyAddress);
            this.Controls.Add(this.textBoxCompanyAddress);
            this.Controls.Add(this.textBoxCompanyName);
            this.Controls.Add(this.labelCompanyName);
            this.Controls.Add(this.labelCompanyIDValue);
            this.Controls.Add(this.labelCompanyID);
            this.Controls.Add(this.helpMenuStrip);
            this.MainMenuStrip = this.helpMenuStrip;
            this.Name = "frmEditCompanyInfo";
            this.Text = "Edit Company Information";
            this.helpMenuStrip.ResumeLayout(false);
            this.helpMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip helpMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label labelCompanyID;
        private System.Windows.Forms.TextBox textBoxCompanyPhone;
        private System.Windows.Forms.TextBox textBoxCompanyEmail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelCompanyPhone;
        private System.Windows.Forms.Label labelCompanyAddress;
        private System.Windows.Forms.TextBox textBoxCompanyAddress;
        private System.Windows.Forms.TextBox textBoxCompanyName;
        private System.Windows.Forms.Label labelCompanyName;
        private System.Windows.Forms.Label labelCompanyIDValue;
        private System.Windows.Forms.Button buttonUpdateCompanyInfo;
        private System.Windows.Forms.TextBox textBoxConsoleLog;
        private System.Windows.Forms.Label labelMessageLog;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoicing_Application
{
    public partial class frmEditCompanyInfo : Form
    {
        //String value to identify the table name 
        private String tableCompany = "Company";

        //String values to identify the columns of the Company Table
        private String companyID = "Company_ID";
        private String companyName = "Company_Name";
        private String companyAddress = "Company_Address";
        private String companyPhone = "Company_Phone";
        private String companyEmail = "Company_Email";
        
        //Boolean Flags to identify which text box is empty 
        private Boolean companyNameEmpty = false;
        private Boolean companyAddressEmpty = false;
        private Boolean phoneNumberEmpty = false;
        private Boolean emailAddressEmpty = false;

        public frmEditCompanyInfo()
        {
            InitializeComponent();
            labelCompanyIDValue.Text = databaseOperationsManager.Current.retrieveColumnData(tableCompany, companyID);
            textBoxCompanyName.Text = databaseOperationsManager.Current.retrieveColumnData(tableCompany, companyName);
            textBoxCompanyAddress.Text = databaseOperationsManager.Current.retrieveColumnData(tableCompany, companyAddress);
            textBoxCompanyPhone.Text = databaseOperationsManager.Current.retrieveColumnData(tableCompany, companyPhone);
            textBoxCompanyEmail.Text = databaseOperationsManager.Current.retrieveColumnData(tableCompany, companyEmail);
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Help Button Clicked");
            MessageBox.Show("The system will display the company info you have submitted into the system.\n\n" +
                "If you need to edit any info, edit the info the textboxes and press update comapny info", "How to use this function");
        }

        private void buttonUpdateCompanyInfo_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Update Company Button Clicked");
            checkTextBoxes();

            if (!companyNameEmpty && !companyAddressEmpty && !phoneNumberEmpty && !emailAddressEmpty)
            {
                Console.WriteLine("All Information present! Checking if new info is present");
                if(checkForDataToUpdate())
                {
                    Console.WriteLine("New Data found updating column");
                    updateData();
                }

                else
                {
                    Console.WriteLine("No Data to update");
                }
            }

            else
            {
                Console.WriteLine("One or more text boxes are empty displaying on message log");
                textBoxConsoleLog.Text = setMessageLog();
            }
        }
   
        //Method designed to update the data
        public void updateData()
        {
            List<String> columnNames = new List<string>() {companyName,companyAddress,companyPhone,companyEmail};
            List<String> columnValues = new List<string>() {textBoxCompanyName.Text,textBoxCompanyAddress.Text,textBoxCompanyPhone.Text,textBoxCompanyEmail.Text};
            databaseOperationsManager.Current.updateRow(tableCompany, companyID, labelCompanyIDValue.Text, columnNames, columnValues);
        }
                
        //Method designed to ensure all required text boxes have a value in it 
        public void checkTextBoxes()
        {
            companyNameEmpty = false;
            companyAddressEmpty = false;
            phoneNumberEmpty = false;
            emailAddressEmpty = false;

            if (String.IsNullOrEmpty(textBoxCompanyName.Text))
            {
                Console.WriteLine("No Company Name Entered");
                companyNameEmpty = true;
            }

            if (String.IsNullOrEmpty(textBoxCompanyAddress.Text))
            {
                Console.WriteLine("No Address Entered");
                companyAddressEmpty = true;
            }

            if (String.IsNullOrEmpty(textBoxCompanyPhone.Text))
            {
                Console.WriteLine("No Phone Number Entered");
                phoneNumberEmpty = true;
            }

            if (String.IsNullOrEmpty(textBoxCompanyEmail.Text))
            {
                Console.WriteLine("No Email Address Entered");
                emailAddressEmpty = true;
            }
        }

        //Method designed to display messages on the console log
        public string setMessageLog()
        {
            string messageLog = "";

            if (companyNameEmpty)
            {
                messageLog = messageLog + "No Company Name Entered! ";
            }

            if (companyAddressEmpty)
            {
                messageLog = messageLog + "No Address Entered! ";
            }

            if (phoneNumberEmpty)
            {
                messageLog = messageLog + "No Phone Number Entered! ";
            }

            if (emailAddressEmpty)
            {
                messageLog = messageLog + "No Email Address Entered! ";
            }

            return messageLog;
        }

        //Method to check for new data
        public Boolean checkForDataToUpdate()
        {
            if(databaseOperationsManager.Current.checkDataColumn(tableCompany,companyName,textBoxCompanyName.Text,companyID,labelCompanyIDValue.Text))
            {
                return true;
            }

            else if (databaseOperationsManager.Current.checkDataColumn(tableCompany, companyAddress, textBoxCompanyAddress.Text, companyID, labelCompanyIDValue.Text))
            {
                return true;
            }

            else if (databaseOperationsManager.Current.checkDataColumn(tableCompany, companyPhone, textBoxCompanyPhone.Text, companyID, labelCompanyIDValue.Text))
            {
                return true;
            }

            else if (databaseOperationsManager.Current.checkDataColumn(tableCompany, companyEmail, textBoxCompanyEmail.Text, companyID, labelCompanyIDValue.Text))
            {
                return true;
            }

            else
            {
                return false;
            }

        }

    }
}

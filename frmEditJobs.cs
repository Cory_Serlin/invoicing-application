﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace Invoicing_Application
{
    public partial class frmEditJobs : Form
    {
        //String and String lists to identify the table name, columns and values
        private string tableName = "Job";
        private List<String> columnNames = new List<string>() {"Job_Name", "Job_Cost" };
        private List<String> columnValues;

        //Boolean Flags to identify which text box is empty
        private Boolean jobNameEmpty = false;
        private Boolean jobCostEmpty = false;

        public frmEditJobs()
        {
            InitializeComponent();
            dataGridViewListofJobs.DataSource = databaseOperationsManager.Current.displayData(tableName);
            columnValues = new List<string>();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Help Button Pressed");
            MessageBox.Show("Select a job you have created using the datagrid to the right.\n" +
                "Information will be provided to you into the textboxes to the left.\n" +
                "Once you've edited your information press submit job change to save your changes\n" +
                "Refresh records list will refresh the grid when needed", "How to use this function");
        }

        private void buttonEditJob_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Submit Record Changes Pressed");
            checkTextBoxes();

            if (!jobNameEmpty && !jobCostEmpty)
            {
                Console.WriteLine("All Information present! Checking for data to update");
                if(checkForDataToUpdate())
                {
                    Console.WriteLine("Records Availiable to update updating now");
                    updateData();
                    dataGridViewListofJobs.DataSource = databaseOperationsManager.Current.displayData("Job");

                }

                else
                {
                    textBoxConsoleLog.Text = "No Data to update";
                }
            }

            else
            {
                Console.WriteLine("One or more text boxes are empty displaying on message log");
                textBoxConsoleLog.Text = setMessageLog();
            }
        }

        private void updateData()
        {
            columnValues.Add(textBoxJobName.Text);
            columnValues.Add(textBoxJobCost.Text);
            databaseOperationsManager.Current.updateRow(tableName, "Job_ID", labelJobIDValue.Text, columnNames, columnValues);
            columnValues.Clear();
        }

        private bool checkForDataToUpdate()
        {
            if(databaseOperationsManager.Current.checkDataColumn(tableName,"Job_Name",textBoxJobName.Text,"Job_ID",labelJobIDValue.Text))
            {
                return true;
            }

            if(databaseOperationsManager.Current.checkDataColumn(tableName,"Job_Cost",textBoxJobCost.Text,"Job_ID",labelJobIDValue.Text))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        private void buttonRefreshRecords_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Refresing Records");
            dataGridViewListofJobs.DataSource = databaseOperationsManager.Current.displayData("Job");
        }

        //Method to display all the records of jobs
        private void dataGridViewListofJobs_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Cell Clicked at row " + dataGridViewListofJobs.CurrentCell.RowIndex);
            int index = dataGridViewListofJobs.CurrentCell.RowIndex;
            DataGridViewRow selectedRow = dataGridViewListofJobs.Rows[index];

            labelJobIDValue.Text = selectedRow.Cells[0].Value.ToString();
            textBoxJobName.Text = selectedRow.Cells[1].Value.ToString();
            textBoxJobCost.Text = selectedRow.Cells[2].Value.ToString();
            labelJobCostasCurrency.Text = float.Parse(textBoxJobCost.Text).ToString("C", new CultureInfo("en-GB"));
        }

        //Method to check which text boxes are empty
        private void checkTextBoxes()
        {
            jobNameEmpty = false;
            jobCostEmpty = false;

            if (String.IsNullOrEmpty(textBoxJobName.Text))
            {
                Console.WriteLine("Job Name is empty!");
                jobNameEmpty = true;
            }

            if (String.IsNullOrEmpty(textBoxJobCost.Text))
            {
                Console.WriteLine("Job Cost is empty!");
                jobCostEmpty = true;
            }
        }

        //Method to set the error messages in the console log 
        public string setMessageLog()
        {
            string messsageLog = "";

            if (jobNameEmpty)
            {
                messsageLog = messsageLog + "No Job Name Entered";
            }

            if (jobCostEmpty)
            {
                messsageLog = messsageLog + "No Job Cost Entered";
            }

            return messsageLog;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace Invoicing_Application
{
    public partial class frmEnterJobInfo : Form
    {
        //Int value representing the ID of the new job
        private int ID = 0;

        //String and String Lists representing the table name, column names and column values
        private String tableName = "Job";
        private List<String> columnNames = new List<string>() {"Job_ID","Job_Name","Job_Cost"};
        private List<String> columnValues;

        //Boolean Flags to identify which text box is empty
        private Boolean jobNameEmpty = false;
        private Boolean jobCostEmpty = false;

        public frmEnterJobInfo()
        {
            InitializeComponent();
            ID = databaseOperationsManager.Current.IDGenerator();
            labelJobIDValue.Text = ID.ToString();
            columnValues = new List<string>();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Help Button Clicked");
            MessageBox.Show("Enter the job name and cost into the text boxes below and press the " +
                " create new job button to add the job to the record!", "How to use this function!");
        }

        private void buttonCreateNewJob_Click(object sender, EventArgs e)
        {
            Console.WriteLine("New Job Button Clicked");
            textBoxConsoleLog.Text = "";
            checkTextBoxes();

            if (!jobNameEmpty && !jobCostEmpty)
            {
                Console.WriteLine("All Information present! Adding Job Information");
                textBoxConsoleLog.Text = "All Job Values Entered";

                columnValues.Add(labelJobIDValue.Text);
                columnValues.Add(textBoxJobName.Text);
                columnValues.Add(textBoxJobCost.Text);

                databaseOperationsManager.Current.addToTable(tableName, columnNames, columnValues);
                columnValues.Clear();

                ID = databaseOperationsManager.Current.IDGenerator();
                labelJobIDValue.Text = ID.ToString();
                textBoxJobName.Text = "";
                textBoxJobCost.Text = "";
            }

            else
            {
                Console.WriteLine("One or more text boxes are empty displaying on message log");
                textBoxConsoleLog.Text = setMessageLog();
            }
        }

        //Method to check which text boxes are empty
        private void checkTextBoxes()
        {
            jobNameEmpty = false;
            jobCostEmpty = false;

            if(String.IsNullOrEmpty(textBoxJobName.Text))
            {
                Console.WriteLine("Job Name is empty!");
                jobNameEmpty = true;
            }

            if(String.IsNullOrEmpty(textBoxJobCost.Text))
            {
                Console.WriteLine("Job Cost is empty!");
                jobCostEmpty = true;
            }
        }

        //Method to set the error messages in the console log 
        public string setMessageLog()
        {
            string messsageLog = "";
            
            if(jobNameEmpty)
            {
                messsageLog = messsageLog + "No Job Name Entered";
            }
            
            if(jobCostEmpty)
            {
                messsageLog = messsageLog + "No Job Cost Entered";
            }
            
            return messsageLog;
        }

        private void textBoxJobCostChanged(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(textBoxJobCost.Text))
            {
                labelJobCostasCurrency.Text = "(Awaiting Input of Cost)";
            }

            else
            {
                try
                {
                    labelJobCostasCurrency.Text = float.Parse(textBoxJobCost.Text).ToString("C", new CultureInfo("en-GB"));
                }
                
                catch(Exception)
                {
                    labelJobCostasCurrency.Text = "(Cost must be a number)";
                }
            }
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Invoicing_Application
{
    /// <summary>
    /// This class is designed to handle the record management of the locally created database.
    /// This class will handle the potential SQL commands that involve writing, retriving and updating records in the database
    /// </summary>
    public class databaseOperationsManager
    {        
        private static Lazy<databaseOperationsManager> _current = new Lazy<databaseOperationsManager>();
        public static databaseOperationsManager Current => _current.Value;

        private SqlConnection connection;

        private SqlDataReader dataReader;
        private string SQLCommand = "";
        private SqlDataAdapter dataAdapter;

        //Constructor Class
        public databaseOperationsManager()
        {

        }

        //Method designed to generate an ID for database tables
        public int IDGenerator()
        {
            int IDValue = 0;

            Random rd = new Random();
            IDValue = rd.Next(1, 10000);
            
            return IDValue;
        }

        //Method designed to generate reports for the user
        public string reportGeneration(String sqlCommand)
        {
            String report = "";
            SQLCommand = sqlCommand;
            connection = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename=" + "C:\\Users\\axel6\\Desktop\\Production Project Workspace\\Invoicing Application\\Invoicing Database.mdf;" + "Integrated Security = " + "True;Connect Timeout = 30");
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = SQLCommand;
            Int32 value = Convert.ToInt32(cmd.ExecuteScalar());
            report = value.ToString();
            connection.Close();
            return report;
        }
        
        //Method designed to display all data from a table 
        public DataTable displayData(String tableName)
        {
            Console.WriteLine("Displaying all records from: " + tableName);
            SQLCommand = "select * from [" + tableName + "]";
            connection = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename=" + "C:\\Users\\axel6\\Desktop\\Production Project Workspace\\Invoicing Application\\Invoicing Database.mdf;" + "Integrated Security = " + "True;Connect Timeout = 30");
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = SQLCommand;
            cmd.ExecuteNonQuery();
            DataTable dataTable = new DataTable();
            dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dataTable);
            connection.Close();

            return dataTable;
        }
        
        //Method designed to add records to tables in the database 
        public void addToTable(String tableName, List<String> columnNames, List<String> columnValues)
        {
            Console.WriteLine("Generating an insert into SQL Command");
            SQLCommand = createAddToSQLCommand(tableName, columnNames, columnValues);
            connection = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename=" + "C:\\Users\\axel6\\Desktop\\Production Project Workspace\\Invoicing Application\\Invoicing Database.mdf;" + "Integrated Security = " + "True;Connect Timeout = 30");
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = SQLCommand;
            cmd.ExecuteNonQuery();
            Console.WriteLine("New Data successfuly added to: " + tableName);
            connection.Close();
        }
    
        //Method designed to generate a select statement for a specific row of data
        public string selectRow(String tableName, String columnName, String IDColumn, String IDValue)
        {
            Console.WriteLine("Getting row from selected data");
            String rowData = "";
            SQLCommand = "Select " + columnName + " from [" + tableName + "] where " + IDColumn + " = '" + IDValue + "'";
            connection = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename=" + "C:\\Users\\axel6\\Desktop\\Production Project Workspace\\Invoicing Application\\Invoicing Database.mdf;" + "Integrated Security = " + "True;Connect Timeout = 30");
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = SQLCommand;
            dataReader = cmd.ExecuteReader();
            dataReader.Read();
            rowData = dataReader[0].ToString();
            connection.Close();

            return rowData;
        }
        
        //Method designed to update records to tables in the database
        public void updateRow(String tableName, String IDColumn, String IDValue, List<String> columnNames, List<String> columnValues)
        {
            Console.WriteLine("Generating an Update SQL Command");
            SQLCommand = createUpdateRowSQLCommand(tableName, IDColumn, IDValue, columnNames, columnValues);
            connection = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename=" + "C:\\Users\\axel6\\Desktop\\Production Project Workspace\\Invoicing Application\\Invoicing Database.mdf;" + "Integrated Security = " + "True;Connect Timeout = 30");
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = SQLCommand;
            cmd.ExecuteNonQuery();
            Console.WriteLine("Data successfully updated");
            connection.Close();
        }

        //Method designed to build an SQL command that updates a row in the table
        public String createUpdateRowSQLCommand(String tableName, String IDColumn, String IDValue, List<String> columnNames, List<String> columnValues)
        {
            String createdUpdateCommand = "";

            createdUpdateCommand = "update [" + tableName + "] set ";

            for (int i = 0; i < columnNames.Count; i++)
            {
                if(i == columnNames.Count - 1)
                {
                    createdUpdateCommand = createdUpdateCommand + columnNames[i] + " = '" + columnValues[i] + "' ";
                }

                else
                {
                    createdUpdateCommand = createdUpdateCommand + columnNames[i] + " = '" + columnValues[i] + "', ";
                }
            }

            createdUpdateCommand = createdUpdateCommand + "where " + IDColumn + " = '" + IDValue + "'";
            
            return createdUpdateCommand;
        }

        //Method designed to build an SQL command designed to add a row to the table
        public String createAddToSQLCommand(String tableName, List<String> columnNames, List<String> columnValues)
        {
            String createdAddCommand = "";

            createdAddCommand = "insert into [" + tableName + "] ";
            createdAddCommand = createdAddCommand + "(";

            for (int i = 0; i < columnNames.Count; i++)
            {
                if(i == columnNames.Count - 1)
                {
                    createdAddCommand = createdAddCommand + columnNames[i] + ") ";
                }

                else
                {
                    createdAddCommand = createdAddCommand + columnNames[i] + ",";
                }
            }

            createdAddCommand = createdAddCommand + "values (";

            for (int i = 0; i < columnValues.Count; i++)
            {
                if(i == columnNames.Count - 1)
                {
                    createdAddCommand = createdAddCommand + "'" + columnValues[i] + "')";
                }

                else
                {
                    createdAddCommand = createdAddCommand + "'" + columnValues[i] + "',";
                }
            }

            return createdAddCommand;
        }

        //Method designed to return a list of values from a column for a combobox
        public List<String> getColumnValues(List<String> values, String columnName, String tableName)
        {
            SQLCommand = "select " + columnName + " from [" + tableName + "]";
            connection = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename=" + "C:\\Users\\axel6\\Desktop\\Production Project Workspace\\Invoicing Application\\Invoicing Database.mdf;" + "Integrated Security = " + "True;Connect Timeout = 30");
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = SQLCommand;
            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                Console.WriteLine("Line Read");
                String accountName = dataReader.GetString(0);
                values.Add(accountName);
            }
            connection.Close();

            return values;
        }
        
        //Method designed to retreive all data from a column 
        public String retrieveColumnData(String tableName, String tableColumn)
        {
            String columnValues = "";
            connection = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename=" + "C:\\Users\\axel6\\Desktop\\Production Project Workspace\\Invoicing Application\\Invoicing Database.mdf;" + "Integrated Security = " + "True;Connect Timeout = 30");
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "select " + tableColumn + " from [" + tableName + "]";

            dataReader = cmd.ExecuteReader();
            dataReader.Read();

            columnValues = dataReader[0].ToString();
            dataReader.Close();
            connection.Close();

            return columnValues;
        }

        //Method designed to check for potential changes in the data 
        public Boolean checkDataColumn(String tableName, String tableColumn, String columnValue, String IDColumn, String IDValue)
        {
            Boolean newDataPresent = false;
            String companyDataValue = "";
            connection = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename=" + "C:\\Users\\axel6\\Desktop\\Production Project Workspace\\Invoicing Application\\Invoicing Database.mdf;" + "Integrated Security = " + "True;Connect Timeout = 30");
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "select " + tableColumn + " from [" + tableName + "] where " + IDColumn + " = '" + IDValue + "'";

            dataReader = cmd.ExecuteReader();
            dataReader.Read();
            companyDataValue = dataReader[0].ToString();

            if (!companyDataValue.Equals(columnValue))
            {
                newDataPresent = true;
            }

            else
            {
                newDataPresent = false;
            }

            dataReader.Close();
            connection.Close();

            return newDataPresent;
        }

    }
}

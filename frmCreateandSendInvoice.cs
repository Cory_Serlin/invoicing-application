﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.Globalization;

namespace Invoicing_Application
{
    public partial class frmCreateandSendInvoice : Form
    {
        //Int for ID value
        int invoiceID = 0;
        
        //Values for account name/job names
        private List<String> accountNames;
        private List<String> jobNames;

        //Values for tableName, columnNames and columnValues
        private string tableName = "Invoice";
        private List<String> columnNames = new List<string>() { "Invoice_ID", "Invoice_Date", "Invoice_Due_Date", "Payee_ID", "Payer_ID",
                                                                 "Job_ID", "Subtotal", "VAT", "Final_Total", "Payment_Status", "Extra_Info" };
        private List<String> columnValues;

        //Boolean Flags checking the Payer Name and Job Name
        private Boolean payerNameMissing = false;
        private Boolean jobNameMissing = false;


        public frmCreateandSendInvoice()
        {
            InitializeComponent();
            invoiceID = databaseOperationsManager.Current.IDGenerator();
            labelInvoiceIDValue.Text = invoiceID.ToString();
            accountNames = new List<string>();
            jobNames = new List<string>();
            accountNames = databaseOperationsManager.Current.getColumnValues(accountNames, "Account_Name", "Account");
            jobNames = databaseOperationsManager.Current.getColumnValues(jobNames, "Job_Name", "Job");
            FillComboBox(accountNames, comboBoxListOfAccountNames);
            FillComboBox(jobNames, comboBoxListOfJobNames);
            textBoxPayeeIDValue.Text = databaseOperationsManager.Current.retrieveColumnData("Company", "Company_ID");
            columnValues = new List<string>();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Help Button Clicked");
            MessageBox.Show("To operate this function use the date pickers to pick the invoice date and the date the invoice is due to be paid by.\n\n" +
                "Use the drop boxes on your right to insert a Payer and the Job that was done\n\n" +
                "The system will automatically put the account ID of the payer and the subtotal into the system.\n\n" +
                "It will also automatically calculate the VAT charged and the Final Total of the work done\n\n" +
                "NOTE: The client's will need to be filled in manually if you use the 'Sundries' account. In all other cases the Client name will be filled automatically\n\n" +
                "Once you're happy with the details press create invoice and the invoice will be created and sent to the client via email","How to use this function");
        }

        private void buttonCreateInvoice_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Create Invoice Clicked");
            checkComboBoxes();
            
            if(!jobNameMissing && !payerNameMissing)
            {
                addInvoice();
                emailInvoice();
            }

            else
            {
                Console.WriteLine("Missing value displaying on message log");
                textBoxConsoleLog.Text = setMessageLog();
            }

        }

        private string setMessageLog()
        {
            String messageLog = "";

            if (jobNameMissing)
            {
                messageLog = messageLog + "Job Name Missing";
            }

            if (payerNameMissing)
            {
                messageLog = messageLog + "Payer Name Missing";
            }
            return messageLog;
        }

        private void checkComboBoxes()
        {
            jobNameMissing = false;
            payerNameMissing = false;

            if(String.IsNullOrEmpty(comboBoxListOfJobNames.Text))
            {
                Console.WriteLine("Job Name Not put in");
                jobNameMissing = true;
            }

            if (String.IsNullOrEmpty(comboBoxListOfAccountNames.Text))
            {
                Console.WriteLine("Account Name Not put in");
                payerNameMissing = true;
            }
        }

        //Code to work out the logic for getting job/payee names to the combo box
        public void FillComboBox(List<String> values, ComboBox comboBoxToFill)
        {
            for(int i = 0; i < values.Count; i++)
            {
                comboBoxToFill.Items.Add(values[i]);
            }
        }

        private void comboBoxListOfAccountNamesIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("Account Name Index Changed");
            textBoxPayerID.Text = databaseOperationsManager.Current.selectRow("Account", "Account_ID", "Account_Name", comboBoxListOfAccountNames.Text);
            
            if(comboBoxListOfAccountNames.Text.Contains("Sundries"))
            {
                textBoxConsoleLog.Text = "Sundries Account Used, make sure to fill the client name into the appropriate text box";
                textBoxClientName.ReadOnly = false;
            }

            else
            {
                textBoxClientName.Text = comboBoxListOfAccountNames.Text;
                textBoxClientName.ReadOnly = true;
            }
        }

        private void comboBoxListOfJobNamesIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("Job Name Index Changed");
            textBoxJobIDValue.Text = databaseOperationsManager.Current.selectRow("Job", "Job_ID", "Job_Name", comboBoxListOfJobNames.Text);
            textBoxSubtotal.Text = databaseOperationsManager.Current.selectRow("Job", "Job_Cost", "Job_Name", comboBoxListOfJobNames.Text);
            textBoxVAT.Text = calculateVat();
            textBoxFinalTotal.Text = calculateFinalTotal();
            labelSubtotalValue.Text = float.Parse(textBoxSubtotal.Text).ToString("C",new CultureInfo("en-GB"));
            labelVATValue.Text = float.Parse(textBoxVAT.Text).ToString("C", new CultureInfo("en-GB"));
            labelFinalTotalValue.Text = float.Parse(textBoxFinalTotal.Text).ToString("C", new CultureInfo("en-GB"));
        }

        private string calculateFinalTotal()
        {
            float subtotal = float.Parse(textBoxSubtotal.Text);
            float VAT = float.Parse(textBoxVAT.Text);
            float finalTotal = subtotal + VAT;
            String finalTotalToString = finalTotal.ToString();
            return finalTotalToString;
        }

        private string calculateVat()
        {
            float subtotal = float.Parse(textBoxSubtotal.Text);
            float VAT = (float) (subtotal * 0.2);
            String VATtoString = VAT.ToString();
            return VATtoString;
        }

        public void addInvoice()
        {
            columnValues.Add(labelInvoiceIDValue.Text);
            columnValues.Add(dateTimePickerInvoiceDate.Text);
            columnValues.Add(dateTimePickerInvoiceDueDate.Text);
            columnValues.Add(textBoxPayeeIDValue.Text);
            columnValues.Add(textBoxPayerID.Text);
            columnValues.Add(textBoxJobIDValue.Text);
            columnValues.Add(textBoxSubtotal.Text);
            columnValues.Add(textBoxVAT.Text);
            columnValues.Add(textBoxFinalTotal.Text);
            columnValues.Add(textBoxPaymentStatus.Text);

            if(String.IsNullOrEmpty(textBoxClientName.Text))
            {
                columnValues.Add(textBoxClientName.Text);
                textBoxConsoleLog.Text = "Warning no client name added using name: " + comboBoxListOfAccountNames.Text;
            }

            else
            {
                columnValues.Add(textBoxClientName.Text);
            }

            databaseOperationsManager.Current.addToTable(tableName, columnNames, columnValues);
        }

        public void emailInvoice()
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("flamesdummyemail@gmail.com");
                mail.To.Add("cserlin7@gmail.com");
                mail.Subject = "Your Invoice! (ID: " + labelInvoiceIDValue.Text + ")";
                mail.Body = "Thank you for using our service! Please see the details of your invoice below.\n" +
                            "Invoice Date: " + dateTimePickerInvoiceDate.Text + "\n" +
                            "Invoice Due to be paid by: " + dateTimePickerInvoiceDueDate.Text + "\n" + 
                            "Job Undertaken: " + comboBoxListOfJobNames.Text + "\n" + 
                            "Subtotal: " + float.Parse(textBoxSubtotal.Text).ToString("C",new CultureInfo("en-GB")) + "\n" +
                            "VAT: " + float.Parse(textBoxVAT.Text).ToString("C", new CultureInfo("en-GB")) + "\n" + 
                            "Total To Pay: " + float.Parse(textBoxFinalTotal.Text).ToString("C", new CultureInfo("en-GB"));

                smtpClient.Port = 587;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential("flamesdummyemail@gmail.com", "DivineY@to1");
                smtpClient.EnableSsl = true;

                smtpClient.Send(mail);
                Console.WriteLine("Email Sent!");
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}




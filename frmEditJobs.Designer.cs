﻿
namespace Invoicing_Application
{
    partial class frmEditJobs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.helpMenuStrip = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelJobID = new System.Windows.Forms.Label();
            this.labelJobIDValue = new System.Windows.Forms.Label();
            this.labelJobName = new System.Windows.Forms.Label();
            this.textBoxJobName = new System.Windows.Forms.TextBox();
            this.labelJobCost = new System.Windows.Forms.Label();
            this.textBoxJobCost = new System.Windows.Forms.TextBox();
            this.labelMessageLog = new System.Windows.Forms.Label();
            this.textBoxConsoleLog = new System.Windows.Forms.TextBox();
            this.buttonEditJob = new System.Windows.Forms.Button();
            this.labelListOfJobs = new System.Windows.Forms.Label();
            this.buttonRefreshRecords = new System.Windows.Forms.Button();
            this.dataGridViewListofJobs = new System.Windows.Forms.DataGridView();
            this.labelJobCostasCurrency = new System.Windows.Forms.Label();
            this.helpMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListofJobs)).BeginInit();
            this.SuspendLayout();
            // 
            // helpMenuStrip
            // 
            this.helpMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.helpMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.helpMenuStrip.Name = "helpMenuStrip";
            this.helpMenuStrip.Size = new System.Drawing.Size(800, 24);
            this.helpMenuStrip.TabIndex = 2;
            this.helpMenuStrip.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // labelJobID
            // 
            this.labelJobID.AutoSize = true;
            this.labelJobID.Location = new System.Drawing.Point(12, 40);
            this.labelJobID.Name = "labelJobID";
            this.labelJobID.Size = new System.Drawing.Size(38, 13);
            this.labelJobID.TabIndex = 3;
            this.labelJobID.Text = "Job ID";
            // 
            // labelJobIDValue
            // 
            this.labelJobIDValue.AutoSize = true;
            this.labelJobIDValue.Location = new System.Drawing.Point(56, 40);
            this.labelJobIDValue.Name = "labelJobIDValue";
            this.labelJobIDValue.Size = new System.Drawing.Size(173, 13);
            this.labelJobIDValue.TabIndex = 4;
            this.labelJobIDValue.Text = "*N/A (Select a row in the datagrid)*";
            // 
            // labelJobName
            // 
            this.labelJobName.AutoSize = true;
            this.labelJobName.Location = new System.Drawing.Point(12, 70);
            this.labelJobName.Name = "labelJobName";
            this.labelJobName.Size = new System.Drawing.Size(55, 13);
            this.labelJobName.TabIndex = 5;
            this.labelJobName.Text = "Job Name";
            // 
            // textBoxJobName
            // 
            this.textBoxJobName.Location = new System.Drawing.Point(73, 70);
            this.textBoxJobName.Name = "textBoxJobName";
            this.textBoxJobName.Size = new System.Drawing.Size(243, 20);
            this.textBoxJobName.TabIndex = 6;
            // 
            // labelJobCost
            // 
            this.labelJobCost.AutoSize = true;
            this.labelJobCost.Location = new System.Drawing.Point(12, 116);
            this.labelJobCost.Name = "labelJobCost";
            this.labelJobCost.Size = new System.Drawing.Size(48, 13);
            this.labelJobCost.TabIndex = 9;
            this.labelJobCost.Text = "Job Cost";
            // 
            // textBoxJobCost
            // 
            this.textBoxJobCost.Location = new System.Drawing.Point(73, 116);
            this.textBoxJobCost.Name = "textBoxJobCost";
            this.textBoxJobCost.Size = new System.Drawing.Size(122, 20);
            this.textBoxJobCost.TabIndex = 10;
            // 
            // labelMessageLog
            // 
            this.labelMessageLog.AutoSize = true;
            this.labelMessageLog.Location = new System.Drawing.Point(12, 208);
            this.labelMessageLog.Name = "labelMessageLog";
            this.labelMessageLog.Size = new System.Drawing.Size(71, 13);
            this.labelMessageLog.TabIndex = 11;
            this.labelMessageLog.Text = "Message Log";
            // 
            // textBoxConsoleLog
            // 
            this.textBoxConsoleLog.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxConsoleLog.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxConsoleLog.Location = new System.Drawing.Point(10, 224);
            this.textBoxConsoleLog.Multiline = true;
            this.textBoxConsoleLog.Name = "textBoxConsoleLog";
            this.textBoxConsoleLog.ReadOnly = true;
            this.textBoxConsoleLog.Size = new System.Drawing.Size(306, 176);
            this.textBoxConsoleLog.TabIndex = 17;
            // 
            // buttonEditJob
            // 
            this.buttonEditJob.Location = new System.Drawing.Point(655, 415);
            this.buttonEditJob.Name = "buttonEditJob";
            this.buttonEditJob.Size = new System.Drawing.Size(133, 23);
            this.buttonEditJob.TabIndex = 18;
            this.buttonEditJob.Text = "Submit Job Change";
            this.buttonEditJob.UseVisualStyleBackColor = true;
            this.buttonEditJob.Click += new System.EventHandler(this.buttonEditJob_Click);
            // 
            // labelListOfJobs
            // 
            this.labelListOfJobs.AutoSize = true;
            this.labelListOfJobs.Location = new System.Drawing.Point(319, 40);
            this.labelListOfJobs.Name = "labelListOfJobs";
            this.labelListOfJobs.Size = new System.Drawing.Size(98, 13);
            this.labelListOfJobs.TabIndex = 19;
            this.labelListOfJobs.Text = "List of Job Records";
            // 
            // buttonRefreshRecords
            // 
            this.buttonRefreshRecords.Location = new System.Drawing.Point(522, 415);
            this.buttonRefreshRecords.Name = "buttonRefreshRecords";
            this.buttonRefreshRecords.Size = new System.Drawing.Size(127, 23);
            this.buttonRefreshRecords.TabIndex = 20;
            this.buttonRefreshRecords.Text = "Refresh Records List";
            this.buttonRefreshRecords.UseVisualStyleBackColor = true;
            this.buttonRefreshRecords.Click += new System.EventHandler(this.buttonRefreshRecords_Click);
            // 
            // dataGridViewListofJobs
            // 
            this.dataGridViewListofJobs.AllowUserToAddRows = false;
            this.dataGridViewListofJobs.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridViewListofJobs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListofJobs.Location = new System.Drawing.Point(322, 70);
            this.dataGridViewListofJobs.Name = "dataGridViewListofJobs";
            this.dataGridViewListofJobs.Size = new System.Drawing.Size(466, 330);
            this.dataGridViewListofJobs.TabIndex = 21;
            this.dataGridViewListofJobs.Click += new System.EventHandler(this.dataGridViewListofJobs_Click);
            // 
            // labelJobCostasCurrency
            // 
            this.labelJobCostasCurrency.AutoSize = true;
            this.labelJobCostasCurrency.Location = new System.Drawing.Point(201, 119);
            this.labelJobCostasCurrency.Name = "labelJobCostasCurrency";
            this.labelJobCostasCurrency.Size = new System.Drawing.Size(115, 13);
            this.labelJobCostasCurrency.TabIndex = 22;
            this.labelJobCostasCurrency.Text = "(Awaiting job selection)";
            // 
            // frmEditJobs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelJobCostasCurrency);
            this.Controls.Add(this.dataGridViewListofJobs);
            this.Controls.Add(this.buttonRefreshRecords);
            this.Controls.Add(this.labelListOfJobs);
            this.Controls.Add(this.buttonEditJob);
            this.Controls.Add(this.textBoxConsoleLog);
            this.Controls.Add(this.labelMessageLog);
            this.Controls.Add(this.textBoxJobCost);
            this.Controls.Add(this.labelJobCost);
            this.Controls.Add(this.textBoxJobName);
            this.Controls.Add(this.labelJobName);
            this.Controls.Add(this.labelJobIDValue);
            this.Controls.Add(this.labelJobID);
            this.Controls.Add(this.helpMenuStrip);
            this.Name = "frmEditJobs";
            this.Text = "Edit Job Information";
            this.helpMenuStrip.ResumeLayout(false);
            this.helpMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListofJobs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip helpMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label labelJobID;
        private System.Windows.Forms.Label labelJobIDValue;
        private System.Windows.Forms.Label labelJobName;
        private System.Windows.Forms.TextBox textBoxJobName;
        private System.Windows.Forms.Label labelJobCost;
        private System.Windows.Forms.TextBox textBoxJobCost;
        private System.Windows.Forms.Label labelMessageLog;
        private System.Windows.Forms.TextBox textBoxConsoleLog;
        private System.Windows.Forms.Button buttonEditJob;
        private System.Windows.Forms.Label labelListOfJobs;
        private System.Windows.Forms.Button buttonRefreshRecords;
        private System.Windows.Forms.DataGridView dataGridViewListofJobs;
        private System.Windows.Forms.Label labelJobCostasCurrency;
    }
}
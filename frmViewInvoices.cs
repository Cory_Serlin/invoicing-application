﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace Invoicing_Application
{
    public partial class frmViewInvoices : Form
    {
        //String and String lists to identify the table name, columns (that can be updated) and values
        private string tableName = "Invoice";
        private List<String> columnNames = new List<string>() { "Payment_Status" };
        private List<String> columnValues;

        public frmViewInvoices()
        {
            InitializeComponent();
            dataGridViewListofInvoices.DataSource = databaseOperationsManager.Current.displayData(tableName);
            columnValues = new List<string>();
        }

        private void buttonEditInvoicePaymentStatus_Click(object sender, EventArgs e)
        {
            textBoxConsoleLog.Text = "";
            Console.WriteLine("Edit Invoice Payment Button clicked!");
            
            if(!String.IsNullOrEmpty(textBoxPaymentStatus.Text))
            {
                Console.WriteLine("Payment Status Present checking to see if can be updated");
                if (!textBoxPaymentStatus.Text.ToLower().Contains("u"))
                {
                    Console.WriteLine("Payment Status Changed! Updating the row");
                    updateData();
                    dataGridViewListofInvoices.DataSource = databaseOperationsManager.Current.displayData(tableName);
                }

                else
                {
                    textBoxConsoleLog.Text = "Payment Status hasn't changed!";
                }
            }

            else
            {
                textBoxConsoleLog.Text = "Payment status isn't filled";
            }
        }

        private void updateData()
        {
            columnValues.Add(textBoxPaymentStatus.Text);
            databaseOperationsManager.Current.updateRow(tableName, "Invoice_ID", labelInvoiceIDValue.Text, columnNames, columnValues);
        }

        private void buttonRefreshRecords_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Refresh Records Button Pressed");
            dataGridViewListofInvoices.DataSource = databaseOperationsManager.Current.displayData(tableName);
        }

        private void dataGridViewListofInvoices_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Console.WriteLine("Cell clicked at row " + dataGridViewListofInvoices.CurrentCell.RowIndex);
            int index = dataGridViewListofInvoices.CurrentCell.RowIndex;
            DataGridViewRow selectedRow = dataGridViewListofInvoices.Rows[index];

            textBoxConsoleLog.Text = "";
            labelInvoiceIDValue.Text = selectedRow.Cells[0].Value.ToString();
            dateTimePickerInvoiceDate.Text = selectedRow.Cells[1].Value.ToString();
            dateTimePickerInvoiceDueDate.Text = selectedRow.Cells[2].Value.ToString();
            textBoxPayeeIDValue.Text = selectedRow.Cells[3].Value.ToString();
            textBoxPayerID.Text = selectedRow.Cells[4].Value.ToString();
            labelClientName.Text = "(" + selectedRow.Cells[10].Value.ToString() + ")";
            textBoxJobIDValue.Text = selectedRow.Cells[5].Value.ToString();
            labelJobName.Text = "(" + databaseOperationsManager.Current.selectRow("Job", "Job_Name", "Job_ID", textBoxJobIDValue.Text) + ")";
            textBoxSubtotal.Text = selectedRow.Cells[6].Value.ToString();
            textBoxVAT.Text = selectedRow.Cells[7].Value.ToString();
            textBoxFinalTotal.Text = selectedRow.Cells[8].Value.ToString();
            labelSubtotalValue.Text = float.Parse(textBoxSubtotal.Text).ToString("C", new CultureInfo("en-GB"));
            labelVATValue.Text = float.Parse(textBoxVAT.Text).ToString("C", new CultureInfo("en-GB"));
            labelFinalTotalValue.Text = float.Parse(textBoxFinalTotal.Text).ToString("C", new CultureInfo("en-GB"));
            textBoxPaymentStatus.Text = selectedRow.Cells[9].Value.ToString();
            checkInvoiceDueDate();
        }

        private void checkInvoiceDueDate()
        {
            if(!labelInvoiceIDValue.Text.Contains("Placeholder Value"))
            {
                if(dateTimePickerInvoiceDueDate.Value <= DateTime.Today && textBoxPaymentStatus.Text.Contains("U"))
                {
                    textBoxConsoleLog.Text = ("Alert this invoice is overdue! Please check and resolve this!");
                }
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Help Button Clicked");
            MessageBox.Show("Select an invoice from the data grid on the right." +
                "\nAfter that edit the payment status and press update payment status to change the payment status");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoicing_Application
{
    public class formManager : ApplicationContext
    {
        private static Lazy<formManager> _current = new Lazy<formManager>();
        public static formManager Current => _current.Value;

        //Method to handle the event of created forms closing
        private void onFormClosed(object sender, EventArgs e)
        {
            if(getOpenFormCount() == 0)
            {
                ExitThread();
            }
        }
        
        //Creates new forms 
        public T createForm<T>() where T : Form, new()
        {
            var ret = new T();
            ret.FormClosed += onFormClosed;
            return ret;
        }

        //Gets the current number of open forms
        public int getOpenFormCount()
        {
            return Application.OpenForms.Count;
        }
                
        //Contructor Class
        public formManager()
        {
            var mainForm = createForm<frmMainScreen>();
            mainForm.Show();
        }
    
    
    }
}

﻿
namespace Invoicing_Application
{
    partial class frmEnterCompanyInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.helpMenuStrip = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelCompanyID = new System.Windows.Forms.Label();
            this.labelCompanyIDValue = new System.Windows.Forms.Label();
            this.labelCompanyName = new System.Windows.Forms.Label();
            this.textBoxCompanyName = new System.Windows.Forms.TextBox();
            this.textBoxCompanyAddress = new System.Windows.Forms.TextBox();
            this.labelCompanyAddress = new System.Windows.Forms.Label();
            this.labelCompanyPhone = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxCompanyEmail = new System.Windows.Forms.TextBox();
            this.textBoxCompanyPhone = new System.Windows.Forms.TextBox();
            this.buttonSubmitCompanyInfo = new System.Windows.Forms.Button();
            this.labelMessageLog = new System.Windows.Forms.Label();
            this.textBoxConsoleLog = new System.Windows.Forms.TextBox();
            this.helpMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // helpMenuStrip
            // 
            this.helpMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.helpMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.helpMenuStrip.Name = "helpMenuStrip";
            this.helpMenuStrip.Size = new System.Drawing.Size(340, 24);
            this.helpMenuStrip.TabIndex = 0;
            this.helpMenuStrip.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // labelCompanyID
            // 
            this.labelCompanyID.AutoSize = true;
            this.labelCompanyID.Location = new System.Drawing.Point(15, 57);
            this.labelCompanyID.Name = "labelCompanyID";
            this.labelCompanyID.Size = new System.Drawing.Size(65, 13);
            this.labelCompanyID.TabIndex = 1;
            this.labelCompanyID.Text = "Company ID";
            // 
            // labelCompanyIDValue
            // 
            this.labelCompanyIDValue.AutoSize = true;
            this.labelCompanyIDValue.Location = new System.Drawing.Point(100, 54);
            this.labelCompanyIDValue.Name = "labelCompanyIDValue";
            this.labelCompanyIDValue.Size = new System.Drawing.Size(101, 13);
            this.labelCompanyIDValue.TabIndex = 2;
            this.labelCompanyIDValue.Text = "*Placeholder Value*";
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.AutoSize = true;
            this.labelCompanyName.Location = new System.Drawing.Point(15, 95);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Size = new System.Drawing.Size(82, 13);
            this.labelCompanyName.TabIndex = 3;
            this.labelCompanyName.Text = "Company Name";
            // 
            // textBoxCompanyName
            // 
            this.textBoxCompanyName.Location = new System.Drawing.Point(103, 88);
            this.textBoxCompanyName.Name = "textBoxCompanyName";
            this.textBoxCompanyName.Size = new System.Drawing.Size(225, 20);
            this.textBoxCompanyName.TabIndex = 4;
            // 
            // textBoxCompanyAddress
            // 
            this.textBoxCompanyAddress.Location = new System.Drawing.Point(103, 132);
            this.textBoxCompanyAddress.Name = "textBoxCompanyAddress";
            this.textBoxCompanyAddress.Size = new System.Drawing.Size(225, 20);
            this.textBoxCompanyAddress.TabIndex = 5;
            // 
            // labelCompanyAddress
            // 
            this.labelCompanyAddress.AutoSize = true;
            this.labelCompanyAddress.Location = new System.Drawing.Point(5, 132);
            this.labelCompanyAddress.Name = "labelCompanyAddress";
            this.labelCompanyAddress.Size = new System.Drawing.Size(92, 13);
            this.labelCompanyAddress.TabIndex = 6;
            this.labelCompanyAddress.Text = "Company Address";
            // 
            // labelCompanyPhone
            // 
            this.labelCompanyPhone.AutoSize = true;
            this.labelCompanyPhone.Location = new System.Drawing.Point(19, 184);
            this.labelCompanyPhone.Name = "labelCompanyPhone";
            this.labelCompanyPhone.Size = new System.Drawing.Size(78, 13);
            this.labelCompanyPhone.TabIndex = 7;
            this.labelCompanyPhone.Text = "Phone Number";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 227);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Email Address";
            // 
            // textBoxCompanyEmail
            // 
            this.textBoxCompanyEmail.Location = new System.Drawing.Point(103, 224);
            this.textBoxCompanyEmail.Name = "textBoxCompanyEmail";
            this.textBoxCompanyEmail.Size = new System.Drawing.Size(225, 20);
            this.textBoxCompanyEmail.TabIndex = 11;
            // 
            // textBoxCompanyPhone
            // 
            this.textBoxCompanyPhone.Location = new System.Drawing.Point(103, 177);
            this.textBoxCompanyPhone.Name = "textBoxCompanyPhone";
            this.textBoxCompanyPhone.Size = new System.Drawing.Size(225, 20);
            this.textBoxCompanyPhone.TabIndex = 12;
            // 
            // buttonSubmitCompanyInfo
            // 
            this.buttonSubmitCompanyInfo.Location = new System.Drawing.Point(207, 368);
            this.buttonSubmitCompanyInfo.Name = "buttonSubmitCompanyInfo";
            this.buttonSubmitCompanyInfo.Size = new System.Drawing.Size(127, 23);
            this.buttonSubmitCompanyInfo.TabIndex = 13;
            this.buttonSubmitCompanyInfo.Text = "Submit Company Info";
            this.buttonSubmitCompanyInfo.UseVisualStyleBackColor = true;
            this.buttonSubmitCompanyInfo.Click += new System.EventHandler(this.submitCompanyInfoButtonOnClick);
            // 
            // labelMessageLog
            // 
            this.labelMessageLog.AutoSize = true;
            this.labelMessageLog.Location = new System.Drawing.Point(9, 271);
            this.labelMessageLog.Name = "labelMessageLog";
            this.labelMessageLog.Size = new System.Drawing.Size(71, 13);
            this.labelMessageLog.TabIndex = 14;
            this.labelMessageLog.Text = "Message Log";
            // 
            // textBoxConsoleLog
            // 
            this.textBoxConsoleLog.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxConsoleLog.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxConsoleLog.Location = new System.Drawing.Point(8, 288);
            this.textBoxConsoleLog.Multiline = true;
            this.textBoxConsoleLog.Name = "textBoxConsoleLog";
            this.textBoxConsoleLog.ReadOnly = true;
            this.textBoxConsoleLog.Size = new System.Drawing.Size(193, 103);
            this.textBoxConsoleLog.TabIndex = 15;
            // 
            // frmEnterCompanyInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 403);
            this.Controls.Add(this.textBoxConsoleLog);
            this.Controls.Add(this.labelMessageLog);
            this.Controls.Add(this.buttonSubmitCompanyInfo);
            this.Controls.Add(this.textBoxCompanyPhone);
            this.Controls.Add(this.textBoxCompanyEmail);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelCompanyPhone);
            this.Controls.Add(this.labelCompanyAddress);
            this.Controls.Add(this.textBoxCompanyAddress);
            this.Controls.Add(this.textBoxCompanyName);
            this.Controls.Add(this.labelCompanyName);
            this.Controls.Add(this.labelCompanyIDValue);
            this.Controls.Add(this.labelCompanyID);
            this.Controls.Add(this.helpMenuStrip);
            this.MainMenuStrip = this.helpMenuStrip;
            this.Name = "frmEnterCompanyInfo";
            this.Text = "Enter Company Information";
            this.helpMenuStrip.ResumeLayout(false);
            this.helpMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip helpMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label labelCompanyID;
        private System.Windows.Forms.Label labelCompanyIDValue;
        private System.Windows.Forms.Label labelCompanyName;
        private System.Windows.Forms.TextBox textBoxCompanyName;
        private System.Windows.Forms.TextBox textBoxCompanyAddress;
        private System.Windows.Forms.Label labelCompanyAddress;
        private System.Windows.Forms.Label labelCompanyPhone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCompanyEmail;
        private System.Windows.Forms.TextBox textBoxCompanyPhone;
        private System.Windows.Forms.Button buttonSubmitCompanyInfo;
        private System.Windows.Forms.Label labelMessageLog;
        private System.Windows.Forms.TextBox textBoxConsoleLog;
    }
}
﻿
namespace Invoicing_Application
{
    partial class frmCreateandSendInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelInvoiceID = new System.Windows.Forms.Label();
            this.helpMenuStrip = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelInvoiceDate = new System.Windows.Forms.Label();
            this.labelInvoiceDueDate = new System.Windows.Forms.Label();
            this.labelPayeeID = new System.Windows.Forms.Label();
            this.labelPayerID = new System.Windows.Forms.Label();
            this.labelJobID = new System.Windows.Forms.Label();
            this.labelSubtotal = new System.Windows.Forms.Label();
            this.labelPaymentStatus = new System.Windows.Forms.Label();
            this.labelVAT = new System.Windows.Forms.Label();
            this.labelFinalTotal = new System.Windows.Forms.Label();
            this.labelInvoiceIDValue = new System.Windows.Forms.Label();
            this.dateTimePickerInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerInvoiceDueDate = new System.Windows.Forms.DateTimePicker();
            this.textBoxPayeeIDValue = new System.Windows.Forms.TextBox();
            this.textBoxPayerID = new System.Windows.Forms.TextBox();
            this.textBoxJobIDValue = new System.Windows.Forms.TextBox();
            this.textBoxPaymentStatus = new System.Windows.Forms.TextBox();
            this.textBoxFinalTotal = new System.Windows.Forms.TextBox();
            this.textBoxVAT = new System.Windows.Forms.TextBox();
            this.textBoxSubtotal = new System.Windows.Forms.TextBox();
            this.labelPayerName = new System.Windows.Forms.Label();
            this.labelJobName = new System.Windows.Forms.Label();
            this.comboBoxListOfAccountNames = new System.Windows.Forms.ComboBox();
            this.comboBoxListOfJobNames = new System.Windows.Forms.ComboBox();
            this.textBoxConsoleLog = new System.Windows.Forms.TextBox();
            this.labelMessageLog = new System.Windows.Forms.Label();
            this.buttonCreateInvoice = new System.Windows.Forms.Button();
            this.textBoxClientName = new System.Windows.Forms.TextBox();
            this.labelClientName = new System.Windows.Forms.Label();
            this.labelSubtotalValue = new System.Windows.Forms.Label();
            this.labelVATValue = new System.Windows.Forms.Label();
            this.labelFinalTotalValue = new System.Windows.Forms.Label();
            this.helpMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelInvoiceID
            // 
            this.labelInvoiceID.AutoSize = true;
            this.labelInvoiceID.Location = new System.Drawing.Point(12, 35);
            this.labelInvoiceID.Name = "labelInvoiceID";
            this.labelInvoiceID.Size = new System.Drawing.Size(56, 13);
            this.labelInvoiceID.TabIndex = 0;
            this.labelInvoiceID.Text = "Invoice ID";
            // 
            // helpMenuStrip
            // 
            this.helpMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.helpMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.helpMenuStrip.Name = "helpMenuStrip";
            this.helpMenuStrip.Size = new System.Drawing.Size(800, 24);
            this.helpMenuStrip.TabIndex = 3;
            this.helpMenuStrip.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // labelInvoiceDate
            // 
            this.labelInvoiceDate.AutoSize = true;
            this.labelInvoiceDate.Location = new System.Drawing.Point(12, 66);
            this.labelInvoiceDate.Name = "labelInvoiceDate";
            this.labelInvoiceDate.Size = new System.Drawing.Size(68, 13);
            this.labelInvoiceDate.TabIndex = 4;
            this.labelInvoiceDate.Text = "Invoice Date";
            // 
            // labelInvoiceDueDate
            // 
            this.labelInvoiceDueDate.AutoSize = true;
            this.labelInvoiceDueDate.Location = new System.Drawing.Point(12, 91);
            this.labelInvoiceDueDate.Name = "labelInvoiceDueDate";
            this.labelInvoiceDueDate.Size = new System.Drawing.Size(91, 13);
            this.labelInvoiceDueDate.TabIndex = 5;
            this.labelInvoiceDueDate.Text = "Invoice Due Date";
            // 
            // labelPayeeID
            // 
            this.labelPayeeID.AutoSize = true;
            this.labelPayeeID.Location = new System.Drawing.Point(29, 117);
            this.labelPayeeID.Name = "labelPayeeID";
            this.labelPayeeID.Size = new System.Drawing.Size(51, 13);
            this.labelPayeeID.TabIndex = 6;
            this.labelPayeeID.Text = "Payee ID";
            // 
            // labelPayerID
            // 
            this.labelPayerID.AutoSize = true;
            this.labelPayerID.Location = new System.Drawing.Point(32, 144);
            this.labelPayerID.Name = "labelPayerID";
            this.labelPayerID.Size = new System.Drawing.Size(48, 13);
            this.labelPayerID.TabIndex = 7;
            this.labelPayerID.Text = "Payer ID";
            // 
            // labelJobID
            // 
            this.labelJobID.AutoSize = true;
            this.labelJobID.Location = new System.Drawing.Point(42, 171);
            this.labelJobID.Name = "labelJobID";
            this.labelJobID.Size = new System.Drawing.Size(38, 13);
            this.labelJobID.TabIndex = 8;
            this.labelJobID.Text = "Job ID";
            // 
            // labelSubtotal
            // 
            this.labelSubtotal.AutoSize = true;
            this.labelSubtotal.Location = new System.Drawing.Point(34, 201);
            this.labelSubtotal.Name = "labelSubtotal";
            this.labelSubtotal.Size = new System.Drawing.Size(46, 13);
            this.labelSubtotal.TabIndex = 9;
            this.labelSubtotal.Text = "Subtotal";
            // 
            // labelPaymentStatus
            // 
            this.labelPaymentStatus.AutoSize = true;
            this.labelPaymentStatus.Location = new System.Drawing.Point(-1, 291);
            this.labelPaymentStatus.Name = "labelPaymentStatus";
            this.labelPaymentStatus.Size = new System.Drawing.Size(81, 13);
            this.labelPaymentStatus.TabIndex = 10;
            this.labelPaymentStatus.Text = "Payment Status";
            // 
            // labelVAT
            // 
            this.labelVAT.AutoSize = true;
            this.labelVAT.Location = new System.Drawing.Point(46, 230);
            this.labelVAT.Name = "labelVAT";
            this.labelVAT.Size = new System.Drawing.Size(34, 13);
            this.labelVAT.TabIndex = 11;
            this.labelVAT.Text = "+VAT";
            // 
            // labelFinalTotal
            // 
            this.labelFinalTotal.AutoSize = true;
            this.labelFinalTotal.Location = new System.Drawing.Point(24, 263);
            this.labelFinalTotal.Name = "labelFinalTotal";
            this.labelFinalTotal.Size = new System.Drawing.Size(56, 13);
            this.labelFinalTotal.TabIndex = 12;
            this.labelFinalTotal.Text = "Final Total";
            // 
            // labelInvoiceIDValue
            // 
            this.labelInvoiceIDValue.AutoSize = true;
            this.labelInvoiceIDValue.Location = new System.Drawing.Point(74, 35);
            this.labelInvoiceIDValue.Name = "labelInvoiceIDValue";
            this.labelInvoiceIDValue.Size = new System.Drawing.Size(101, 13);
            this.labelInvoiceIDValue.TabIndex = 13;
            this.labelInvoiceIDValue.Text = "*Placeholder Value*";
            // 
            // dateTimePickerInvoiceDate
            // 
            this.dateTimePickerInvoiceDate.Location = new System.Drawing.Point(86, 60);
            this.dateTimePickerInvoiceDate.Name = "dateTimePickerInvoiceDate";
            this.dateTimePickerInvoiceDate.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerInvoiceDate.TabIndex = 14;
            // 
            // dateTimePickerInvoiceDueDate
            // 
            this.dateTimePickerInvoiceDueDate.Location = new System.Drawing.Point(110, 91);
            this.dateTimePickerInvoiceDueDate.Name = "dateTimePickerInvoiceDueDate";
            this.dateTimePickerInvoiceDueDate.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerInvoiceDueDate.TabIndex = 15;
            // 
            // textBoxPayeeIDValue
            // 
            this.textBoxPayeeIDValue.Location = new System.Drawing.Point(110, 115);
            this.textBoxPayeeIDValue.Name = "textBoxPayeeIDValue";
            this.textBoxPayeeIDValue.ReadOnly = true;
            this.textBoxPayeeIDValue.Size = new System.Drawing.Size(100, 20);
            this.textBoxPayeeIDValue.TabIndex = 16;
            // 
            // textBoxPayerID
            // 
            this.textBoxPayerID.Location = new System.Drawing.Point(110, 142);
            this.textBoxPayerID.Name = "textBoxPayerID";
            this.textBoxPayerID.ReadOnly = true;
            this.textBoxPayerID.Size = new System.Drawing.Size(100, 20);
            this.textBoxPayerID.TabIndex = 17;
            // 
            // textBoxJobIDValue
            // 
            this.textBoxJobIDValue.Location = new System.Drawing.Point(110, 167);
            this.textBoxJobIDValue.Name = "textBoxJobIDValue";
            this.textBoxJobIDValue.ReadOnly = true;
            this.textBoxJobIDValue.Size = new System.Drawing.Size(100, 20);
            this.textBoxJobIDValue.TabIndex = 18;
            // 
            // textBoxPaymentStatus
            // 
            this.textBoxPaymentStatus.Location = new System.Drawing.Point(110, 289);
            this.textBoxPaymentStatus.Name = "textBoxPaymentStatus";
            this.textBoxPaymentStatus.ReadOnly = true;
            this.textBoxPaymentStatus.Size = new System.Drawing.Size(100, 20);
            this.textBoxPaymentStatus.TabIndex = 19;
            this.textBoxPaymentStatus.Text = "U";
            // 
            // textBoxFinalTotal
            // 
            this.textBoxFinalTotal.Location = new System.Drawing.Point(110, 256);
            this.textBoxFinalTotal.Name = "textBoxFinalTotal";
            this.textBoxFinalTotal.ReadOnly = true;
            this.textBoxFinalTotal.Size = new System.Drawing.Size(100, 20);
            this.textBoxFinalTotal.TabIndex = 20;
            this.textBoxFinalTotal.Visible = false;
            // 
            // textBoxVAT
            // 
            this.textBoxVAT.Location = new System.Drawing.Point(110, 227);
            this.textBoxVAT.Name = "textBoxVAT";
            this.textBoxVAT.ReadOnly = true;
            this.textBoxVAT.Size = new System.Drawing.Size(100, 20);
            this.textBoxVAT.TabIndex = 21;
            this.textBoxVAT.Visible = false;
            // 
            // textBoxSubtotal
            // 
            this.textBoxSubtotal.Location = new System.Drawing.Point(110, 198);
            this.textBoxSubtotal.Name = "textBoxSubtotal";
            this.textBoxSubtotal.ReadOnly = true;
            this.textBoxSubtotal.Size = new System.Drawing.Size(100, 20);
            this.textBoxSubtotal.TabIndex = 22;
            this.textBoxSubtotal.Visible = false;
            // 
            // labelPayerName
            // 
            this.labelPayerName.AutoSize = true;
            this.labelPayerName.Location = new System.Drawing.Point(551, 38);
            this.labelPayerName.Name = "labelPayerName";
            this.labelPayerName.Size = new System.Drawing.Size(65, 13);
            this.labelPayerName.TabIndex = 23;
            this.labelPayerName.Text = "Payer Name";
            // 
            // labelJobName
            // 
            this.labelJobName.AutoSize = true;
            this.labelJobName.Location = new System.Drawing.Point(561, 77);
            this.labelJobName.Name = "labelJobName";
            this.labelJobName.Size = new System.Drawing.Size(55, 13);
            this.labelJobName.TabIndex = 24;
            this.labelJobName.Text = "Job Name";
            // 
            // comboBoxListOfAccountNames
            // 
            this.comboBoxListOfAccountNames.FormattingEnabled = true;
            this.comboBoxListOfAccountNames.Location = new System.Drawing.Point(622, 35);
            this.comboBoxListOfAccountNames.Name = "comboBoxListOfAccountNames";
            this.comboBoxListOfAccountNames.Size = new System.Drawing.Size(121, 21);
            this.comboBoxListOfAccountNames.TabIndex = 25;
            this.comboBoxListOfAccountNames.SelectedIndexChanged += new System.EventHandler(this.comboBoxListOfAccountNamesIndexChanged);
            // 
            // comboBoxListOfJobNames
            // 
            this.comboBoxListOfJobNames.FormattingEnabled = true;
            this.comboBoxListOfJobNames.Location = new System.Drawing.Point(622, 74);
            this.comboBoxListOfJobNames.Name = "comboBoxListOfJobNames";
            this.comboBoxListOfJobNames.Size = new System.Drawing.Size(121, 21);
            this.comboBoxListOfJobNames.TabIndex = 26;
            this.comboBoxListOfJobNames.SelectedIndexChanged += new System.EventHandler(this.comboBoxListOfJobNamesIndexChanged);
            // 
            // textBoxConsoleLog
            // 
            this.textBoxConsoleLog.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxConsoleLog.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxConsoleLog.Location = new System.Drawing.Point(248, 250);
            this.textBoxConsoleLog.Multiline = true;
            this.textBoxConsoleLog.Name = "textBoxConsoleLog";
            this.textBoxConsoleLog.ReadOnly = true;
            this.textBoxConsoleLog.Size = new System.Drawing.Size(548, 188);
            this.textBoxConsoleLog.TabIndex = 28;
            // 
            // labelMessageLog
            // 
            this.labelMessageLog.AutoSize = true;
            this.labelMessageLog.Location = new System.Drawing.Point(245, 234);
            this.labelMessageLog.Name = "labelMessageLog";
            this.labelMessageLog.Size = new System.Drawing.Size(71, 13);
            this.labelMessageLog.TabIndex = 27;
            this.labelMessageLog.Text = "Message Log";
            // 
            // buttonCreateInvoice
            // 
            this.buttonCreateInvoice.Location = new System.Drawing.Point(681, 220);
            this.buttonCreateInvoice.Name = "buttonCreateInvoice";
            this.buttonCreateInvoice.Size = new System.Drawing.Size(107, 23);
            this.buttonCreateInvoice.TabIndex = 30;
            this.buttonCreateInvoice.Text = "Create Invoice";
            this.buttonCreateInvoice.UseVisualStyleBackColor = true;
            this.buttonCreateInvoice.Click += new System.EventHandler(this.buttonCreateInvoice_Click);
            // 
            // textBoxClientName
            // 
            this.textBoxClientName.Location = new System.Drawing.Point(110, 325);
            this.textBoxClientName.Name = "textBoxClientName";
            this.textBoxClientName.ReadOnly = true;
            this.textBoxClientName.Size = new System.Drawing.Size(100, 20);
            this.textBoxClientName.TabIndex = 31;
            // 
            // labelClientName
            // 
            this.labelClientName.AutoSize = true;
            this.labelClientName.Location = new System.Drawing.Point(16, 328);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(64, 13);
            this.labelClientName.TabIndex = 32;
            this.labelClientName.Text = "Client Name";
            // 
            // labelSubtotalValue
            // 
            this.labelSubtotalValue.AutoSize = true;
            this.labelSubtotalValue.Location = new System.Drawing.Point(111, 201);
            this.labelSubtotalValue.Name = "labelSubtotalValue";
            this.labelSubtotalValue.Size = new System.Drawing.Size(122, 13);
            this.labelSubtotalValue.TabIndex = 33;
            this.labelSubtotalValue.Text = "*Awaiting Job Selection*";
            // 
            // labelVATValue
            // 
            this.labelVATValue.AutoSize = true;
            this.labelVATValue.Location = new System.Drawing.Point(110, 230);
            this.labelVATValue.Name = "labelVATValue";
            this.labelVATValue.Size = new System.Drawing.Size(122, 13);
            this.labelVATValue.TabIndex = 34;
            this.labelVATValue.Text = "*Awaiting Job Selection*";
            // 
            // labelFinalTotalValue
            // 
            this.labelFinalTotalValue.AutoSize = true;
            this.labelFinalTotalValue.Location = new System.Drawing.Point(110, 262);
            this.labelFinalTotalValue.Name = "labelFinalTotalValue";
            this.labelFinalTotalValue.Size = new System.Drawing.Size(122, 13);
            this.labelFinalTotalValue.TabIndex = 35;
            this.labelFinalTotalValue.Text = "*Awaiting Job Selection*";
            // 
            // frmCreateandSendInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelFinalTotalValue);
            this.Controls.Add(this.labelVATValue);
            this.Controls.Add(this.labelSubtotalValue);
            this.Controls.Add(this.labelClientName);
            this.Controls.Add(this.textBoxClientName);
            this.Controls.Add(this.buttonCreateInvoice);
            this.Controls.Add(this.textBoxConsoleLog);
            this.Controls.Add(this.labelMessageLog);
            this.Controls.Add(this.comboBoxListOfJobNames);
            this.Controls.Add(this.comboBoxListOfAccountNames);
            this.Controls.Add(this.labelJobName);
            this.Controls.Add(this.labelPayerName);
            this.Controls.Add(this.textBoxSubtotal);
            this.Controls.Add(this.textBoxVAT);
            this.Controls.Add(this.textBoxFinalTotal);
            this.Controls.Add(this.textBoxPaymentStatus);
            this.Controls.Add(this.textBoxJobIDValue);
            this.Controls.Add(this.textBoxPayerID);
            this.Controls.Add(this.textBoxPayeeIDValue);
            this.Controls.Add(this.dateTimePickerInvoiceDueDate);
            this.Controls.Add(this.dateTimePickerInvoiceDate);
            this.Controls.Add(this.labelInvoiceIDValue);
            this.Controls.Add(this.labelFinalTotal);
            this.Controls.Add(this.labelVAT);
            this.Controls.Add(this.labelPaymentStatus);
            this.Controls.Add(this.labelSubtotal);
            this.Controls.Add(this.labelJobID);
            this.Controls.Add(this.labelPayerID);
            this.Controls.Add(this.labelPayeeID);
            this.Controls.Add(this.labelInvoiceDueDate);
            this.Controls.Add(this.labelInvoiceDate);
            this.Controls.Add(this.helpMenuStrip);
            this.Controls.Add(this.labelInvoiceID);
            this.Name = "frmCreateandSendInvoice";
            this.Text = "Create and Send Invoices";
            this.helpMenuStrip.ResumeLayout(false);
            this.helpMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelInvoiceID;
        private System.Windows.Forms.MenuStrip helpMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label labelInvoiceDate;
        private System.Windows.Forms.Label labelInvoiceDueDate;
        private System.Windows.Forms.Label labelPayeeID;
        private System.Windows.Forms.Label labelPayerID;
        private System.Windows.Forms.Label labelJobID;
        private System.Windows.Forms.Label labelSubtotal;
        private System.Windows.Forms.Label labelPaymentStatus;
        private System.Windows.Forms.Label labelVAT;
        private System.Windows.Forms.Label labelFinalTotal;
        private System.Windows.Forms.Label labelInvoiceIDValue;
        private System.Windows.Forms.DateTimePicker dateTimePickerInvoiceDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerInvoiceDueDate;
        private System.Windows.Forms.TextBox textBoxPayeeIDValue;
        private System.Windows.Forms.TextBox textBoxPayerID;
        private System.Windows.Forms.TextBox textBoxJobIDValue;
        private System.Windows.Forms.TextBox textBoxPaymentStatus;
        private System.Windows.Forms.TextBox textBoxFinalTotal;
        private System.Windows.Forms.TextBox textBoxVAT;
        private System.Windows.Forms.TextBox textBoxSubtotal;
        private System.Windows.Forms.Label labelPayerName;
        private System.Windows.Forms.Label labelJobName;
        private System.Windows.Forms.ComboBox comboBoxListOfAccountNames;
        private System.Windows.Forms.ComboBox comboBoxListOfJobNames;
        private System.Windows.Forms.TextBox textBoxConsoleLog;
        private System.Windows.Forms.Label labelMessageLog;
        private System.Windows.Forms.Button buttonCreateInvoice;
        private System.Windows.Forms.TextBox textBoxClientName;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.Label labelSubtotalValue;
        private System.Windows.Forms.Label labelVATValue;
        private System.Windows.Forms.Label labelFinalTotalValue;
    }
}
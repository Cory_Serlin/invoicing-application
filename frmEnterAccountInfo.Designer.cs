﻿
namespace Invoicing_Application
{
    partial class frmEnterAccountInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.helpMenuStrip = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelAccountID = new System.Windows.Forms.Label();
            this.labelAccountIDValue = new System.Windows.Forms.Label();
            this.labelAccountName = new System.Windows.Forms.Label();
            this.textBoxAccountName = new System.Windows.Forms.TextBox();
            this.labelAccountPhone = new System.Windows.Forms.Label();
            this.textBoxAccountPhone = new System.Windows.Forms.TextBox();
            this.textBoxAccountEmail = new System.Windows.Forms.TextBox();
            this.labelAccountEmail = new System.Windows.Forms.Label();
            this.labelMessageLog = new System.Windows.Forms.Label();
            this.textBoxConsoleLog = new System.Windows.Forms.TextBox();
            this.buttonCreateNewAccount = new System.Windows.Forms.Button();
            this.helpMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // helpMenuStrip
            // 
            this.helpMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.helpMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.helpMenuStrip.Name = "helpMenuStrip";
            this.helpMenuStrip.Size = new System.Drawing.Size(400, 24);
            this.helpMenuStrip.TabIndex = 2;
            this.helpMenuStrip.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // labelAccountID
            // 
            this.labelAccountID.AutoSize = true;
            this.labelAccountID.Location = new System.Drawing.Point(16, 28);
            this.labelAccountID.Name = "labelAccountID";
            this.labelAccountID.Size = new System.Drawing.Size(61, 13);
            this.labelAccountID.TabIndex = 3;
            this.labelAccountID.Text = "Account ID";
            // 
            // labelAccountIDValue
            // 
            this.labelAccountIDValue.AutoSize = true;
            this.labelAccountIDValue.Location = new System.Drawing.Point(97, 28);
            this.labelAccountIDValue.Name = "labelAccountIDValue";
            this.labelAccountIDValue.Size = new System.Drawing.Size(101, 13);
            this.labelAccountIDValue.TabIndex = 4;
            this.labelAccountIDValue.Text = "*Placeholder Value*";
            // 
            // labelAccountName
            // 
            this.labelAccountName.AutoSize = true;
            this.labelAccountName.Location = new System.Drawing.Point(16, 59);
            this.labelAccountName.Name = "labelAccountName";
            this.labelAccountName.Size = new System.Drawing.Size(78, 13);
            this.labelAccountName.TabIndex = 5;
            this.labelAccountName.Text = "Account Name";
            // 
            // textBoxAccountName
            // 
            this.textBoxAccountName.Location = new System.Drawing.Point(97, 56);
            this.textBoxAccountName.Name = "textBoxAccountName";
            this.textBoxAccountName.Size = new System.Drawing.Size(291, 20);
            this.textBoxAccountName.TabIndex = 6;
            // 
            // labelAccountPhone
            // 
            this.labelAccountPhone.AutoSize = true;
            this.labelAccountPhone.Location = new System.Drawing.Point(17, 90);
            this.labelAccountPhone.Name = "labelAccountPhone";
            this.labelAccountPhone.Size = new System.Drawing.Size(81, 13);
            this.labelAccountPhone.TabIndex = 7;
            this.labelAccountPhone.Text = "Account Phone";
            // 
            // textBoxAccountPhone
            // 
            this.textBoxAccountPhone.Location = new System.Drawing.Point(97, 90);
            this.textBoxAccountPhone.Name = "textBoxAccountPhone";
            this.textBoxAccountPhone.Size = new System.Drawing.Size(291, 20);
            this.textBoxAccountPhone.TabIndex = 8;
            // 
            // textBoxAccountEmail
            // 
            this.textBoxAccountEmail.Location = new System.Drawing.Point(97, 118);
            this.textBoxAccountEmail.Name = "textBoxAccountEmail";
            this.textBoxAccountEmail.Size = new System.Drawing.Size(291, 20);
            this.textBoxAccountEmail.TabIndex = 10;
            // 
            // labelAccountEmail
            // 
            this.labelAccountEmail.AutoSize = true;
            this.labelAccountEmail.Location = new System.Drawing.Point(16, 121);
            this.labelAccountEmail.Name = "labelAccountEmail";
            this.labelAccountEmail.Size = new System.Drawing.Size(75, 13);
            this.labelAccountEmail.TabIndex = 11;
            this.labelAccountEmail.Text = "Account Email";
            // 
            // labelMessageLog
            // 
            this.labelMessageLog.AutoSize = true;
            this.labelMessageLog.Location = new System.Drawing.Point(16, 217);
            this.labelMessageLog.Name = "labelMessageLog";
            this.labelMessageLog.Size = new System.Drawing.Size(71, 13);
            this.labelMessageLog.TabIndex = 13;
            this.labelMessageLog.Text = "Message Log";
            // 
            // textBoxConsoleLog
            // 
            this.textBoxConsoleLog.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxConsoleLog.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxConsoleLog.Location = new System.Drawing.Point(19, 233);
            this.textBoxConsoleLog.Multiline = true;
            this.textBoxConsoleLog.Name = "textBoxConsoleLog";
            this.textBoxConsoleLog.ReadOnly = true;
            this.textBoxConsoleLog.Size = new System.Drawing.Size(369, 176);
            this.textBoxConsoleLog.TabIndex = 17;
            // 
            // buttonCreateNewAccount
            // 
            this.buttonCreateNewAccount.Location = new System.Drawing.Point(265, 415);
            this.buttonCreateNewAccount.Name = "buttonCreateNewAccount";
            this.buttonCreateNewAccount.Size = new System.Drawing.Size(122, 23);
            this.buttonCreateNewAccount.TabIndex = 18;
            this.buttonCreateNewAccount.Text = "Create New Account";
            this.buttonCreateNewAccount.UseVisualStyleBackColor = true;
            this.buttonCreateNewAccount.Click += new System.EventHandler(this.buttonCreateNewAccount_Click);
            // 
            // frmEnterAccountInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 450);
            this.Controls.Add(this.buttonCreateNewAccount);
            this.Controls.Add(this.textBoxConsoleLog);
            this.Controls.Add(this.labelMessageLog);
            this.Controls.Add(this.labelAccountEmail);
            this.Controls.Add(this.textBoxAccountEmail);
            this.Controls.Add(this.textBoxAccountPhone);
            this.Controls.Add(this.labelAccountPhone);
            this.Controls.Add(this.textBoxAccountName);
            this.Controls.Add(this.labelAccountName);
            this.Controls.Add(this.labelAccountIDValue);
            this.Controls.Add(this.labelAccountID);
            this.Controls.Add(this.helpMenuStrip);
            this.Name = "frmEnterAccountInfo";
            this.Text = "Create New Account";
            this.helpMenuStrip.ResumeLayout(false);
            this.helpMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip helpMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label labelAccountID;
        private System.Windows.Forms.Label labelAccountIDValue;
        private System.Windows.Forms.Label labelAccountName;
        private System.Windows.Forms.TextBox textBoxAccountName;
        private System.Windows.Forms.Label labelAccountPhone;
        private System.Windows.Forms.TextBox textBoxAccountPhone;
        private System.Windows.Forms.TextBox textBoxAccountEmail;
        private System.Windows.Forms.Label labelAccountEmail;
        private System.Windows.Forms.Label labelMessageLog;
        private System.Windows.Forms.TextBox textBoxConsoleLog;
        private System.Windows.Forms.Button buttonCreateNewAccount;
    }
}
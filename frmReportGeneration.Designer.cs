﻿
namespace Invoicing_Application
{
    partial class frmReportGeneration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxReportLog = new System.Windows.Forms.TextBox();
            this.comboBoxReportOptions = new System.Windows.Forms.ComboBox();
            this.helpMenuStrip = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelSelectReportTypeTitle = new System.Windows.Forms.Label();
            this.labelCheckJobStatsTitle = new System.Windows.Forms.Label();
            this.comboBoxListOfJobs = new System.Windows.Forms.ComboBox();
            this.labelJobStatsReport = new System.Windows.Forms.Label();
            this.labelJobIDValue = new System.Windows.Forms.Label();
            this.helpMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxReportLog
            // 
            this.textBoxReportLog.Location = new System.Drawing.Point(13, 29);
            this.textBoxReportLog.Multiline = true;
            this.textBoxReportLog.Name = "textBoxReportLog";
            this.textBoxReportLog.Size = new System.Drawing.Size(775, 366);
            this.textBoxReportLog.TabIndex = 0;
            // 
            // comboBoxReportOptions
            // 
            this.comboBoxReportOptions.FormattingEnabled = true;
            this.comboBoxReportOptions.Items.AddRange(new object[] {
            "Financial",
            "Statistical"});
            this.comboBoxReportOptions.Location = new System.Drawing.Point(13, 424);
            this.comboBoxReportOptions.Name = "comboBoxReportOptions";
            this.comboBoxReportOptions.Size = new System.Drawing.Size(775, 21);
            this.comboBoxReportOptions.TabIndex = 1;
            this.comboBoxReportOptions.SelectedIndexChanged += new System.EventHandler(this.comboBoxReportOptionsValueChanged);
            // 
            // helpMenuStrip
            // 
            this.helpMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.helpMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.helpMenuStrip.Name = "helpMenuStrip";
            this.helpMenuStrip.Size = new System.Drawing.Size(800, 24);
            this.helpMenuStrip.TabIndex = 2;
            this.helpMenuStrip.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // labelSelectReportTypeTitle
            // 
            this.labelSelectReportTypeTitle.AutoSize = true;
            this.labelSelectReportTypeTitle.Location = new System.Drawing.Point(13, 402);
            this.labelSelectReportTypeTitle.Name = "labelSelectReportTypeTitle";
            this.labelSelectReportTypeTitle.Size = new System.Drawing.Size(158, 13);
            this.labelSelectReportTypeTitle.TabIndex = 3;
            this.labelSelectReportTypeTitle.Text = "Select Report Type to Generate";
            // 
            // labelCheckJobStatsTitle
            // 
            this.labelCheckJobStatsTitle.AutoSize = true;
            this.labelCheckJobStatsTitle.Location = new System.Drawing.Point(13, 470);
            this.labelCheckJobStatsTitle.Name = "labelCheckJobStatsTitle";
            this.labelCheckJobStatsTitle.Size = new System.Drawing.Size(118, 13);
            this.labelCheckJobStatsTitle.TabIndex = 4;
            this.labelCheckJobStatsTitle.Text = "Check Job Based Stats";
            // 
            // comboBoxListOfJobs
            // 
            this.comboBoxListOfJobs.FormattingEnabled = true;
            this.comboBoxListOfJobs.Location = new System.Drawing.Point(13, 486);
            this.comboBoxListOfJobs.Name = "comboBoxListOfJobs";
            this.comboBoxListOfJobs.Size = new System.Drawing.Size(158, 21);
            this.comboBoxListOfJobs.TabIndex = 5;
            this.comboBoxListOfJobs.SelectedIndexChanged += new System.EventHandler(this.comboBoxListOfJobNamesIndexChanged);
            // 
            // labelJobStatsReport
            // 
            this.labelJobStatsReport.AutoSize = true;
            this.labelJobStatsReport.Location = new System.Drawing.Point(219, 489);
            this.labelJobStatsReport.Name = "labelJobStatsReport";
            this.labelJobStatsReport.Size = new System.Drawing.Size(102, 13);
            this.labelJobStatsReport.TabIndex = 6;
            this.labelJobStatsReport.Text = "*Awaiting Job Input*";
            // 
            // labelJobIDValue
            // 
            this.labelJobIDValue.AutoSize = true;
            this.labelJobIDValue.Location = new System.Drawing.Point(16, 514);
            this.labelJobIDValue.Name = "labelJobIDValue";
            this.labelJobIDValue.Size = new System.Drawing.Size(46, 13);
            this.labelJobIDValue.TabIndex = 7;
            this.labelJobIDValue.Text = "*JOBID*";
            this.labelJobIDValue.Visible = false;
            // 
            // frmReportGeneration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 544);
            this.Controls.Add(this.labelJobIDValue);
            this.Controls.Add(this.labelJobStatsReport);
            this.Controls.Add(this.comboBoxListOfJobs);
            this.Controls.Add(this.labelCheckJobStatsTitle);
            this.Controls.Add(this.labelSelectReportTypeTitle);
            this.Controls.Add(this.helpMenuStrip);
            this.Controls.Add(this.comboBoxReportOptions);
            this.Controls.Add(this.textBoxReportLog);
            this.Name = "frmReportGeneration";
            this.Text = "Reports";
            this.helpMenuStrip.ResumeLayout(false);
            this.helpMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxReportLog;
        private System.Windows.Forms.ComboBox comboBoxReportOptions;
        private System.Windows.Forms.MenuStrip helpMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label labelSelectReportTypeTitle;
        private System.Windows.Forms.Label labelCheckJobStatsTitle;
        private System.Windows.Forms.ComboBox comboBoxListOfJobs;
        private System.Windows.Forms.Label labelJobStatsReport;
        private System.Windows.Forms.Label labelJobIDValue;
    }
}
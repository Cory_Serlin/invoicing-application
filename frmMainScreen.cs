﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoicing_Application
{
    public partial class frmMainScreen : Form
    {        
        public frmMainScreen()
        {
            InitializeComponent();
            countOutstandingandOverdueInvoices();
        }

        private void countOutstandingandOverdueInvoices()
        {
            labelNumberOfOutstandingInvoices.Text = "Current Number of outstanding Invoices: " + databaseOperationsManager.Current.reportGeneration("SELECT COUNT(*) FROM Invoice WHERE Payment_Status = 'U'");
            labelNumberOfOverdueInvoices.Text = "Current Number of overdue invoices: " + databaseOperationsManager.Current.reportGeneration("SELECT COUNT(*) FROM Invoice WHERE Payment_Status = 'U' and CAST(Invoice_Due_Date AS Date) <= CAST(GetDate() AS Date)");
        }

        private void aboutButtonOnClick(object sender, EventArgs e)
        {
            Console.WriteLine("About button Clicked");
            MessageBox.Show("Created by Cory Serlin\n\nVersion 1.0");
        }

        private void navigationButtonClick(object sender, EventArgs e)
        {
            String buttonText = (sender as Button).Text.ToLower();
            Console.WriteLine("The button pressed was: " + buttonText);
            formNavigation(buttonText);

        }

        private void formNavigation(String buttonText)
        {
            var newForm = new Form();

            int companyEntered = int.Parse(databaseOperationsManager.Current.reportGeneration("Select Count(*) From Company"));            

            if(companyEntered == 0 && !buttonText.Contains("enter company info"))
            {
                Console.WriteLine("No Company Data Entered");
                MessageBox.Show("Error No Company Info Stored! Please press enter company info first");
                return;
            }

            switch (buttonText)
            {
                case "edit company info":
                    Console.WriteLine("Navigating to edit company info screen");
                    newForm = formManager.Current.createForm<frmEditCompanyInfo>();
                    newForm.ShowDialog();
                    break;

                case "enter company info":
                    Console.WriteLine("Checking if Company Information has been submitted already");
                    if (companyEntered == 1)
                    {
                        MessageBox.Show("Error: Company has already been entered! Please go to Edit Company Info if you wish to " +
                            "edit your company's information");
                    }

                    else
                    {
                        newForm = formManager.Current.createForm<frmEnterCompanyInfo>();
                        newForm.ShowDialog();
                    }
                    break;

                case "create job":
                    Console.WriteLine("Navigating to create job screen");
                    newForm = formManager.Current.createForm<frmEnterJobInfo>();
                    newForm.ShowDialog();
                    break;

                case "view/edit jobs":
                    Console.WriteLine("Navigating to edit job screen");
                    newForm = formManager.Current.createForm<frmEditJobs>();
                    newForm.ShowDialog();
                    break;

                case "register new client":
                    Console.WriteLine("Navigating to register new client screen");
                    newForm = formManager.Current.createForm<frmEnterAccountInfo>();
                    newForm.ShowDialog();
                    break;

                case "view/edit clients":
                    Console.WriteLine("Navigating to view/edit clients screen");
                    newForm = formManager.Current.createForm<frmEditAccounts>();
                    newForm.ShowDialog();
                    break;

                case "create and send invoices":
                    Console.WriteLine("Navigating to create and send invoices screen");
                    newForm = formManager.Current.createForm<frmCreateandSendInvoice>();
                    newForm.ShowDialog();
                    break;

                case "view invoices":
                    Console.WriteLine("Navigating to view invoices screen");
                    newForm = formManager.Current.createForm<frmViewInvoices>();
                    newForm.ShowDialog();
                    break;

                case "view/generate reports":
                    Console.WriteLine("Navigating to view/generate reports screen");
                    newForm = formManager.Current.createForm<frmReportGeneration>();
                    newForm.ShowDialog();
                    break;
            }

            
        }

        private void buttonRefreshStats_Click(object sender, EventArgs e)
        {
            countOutstandingandOverdueInvoices();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Help Button Clicked");
            MessageBox.Show("Each button in the main menu will navigate you to a form relating to the" +
                "function. \n\nThe program will also show the current number of outstanding and " +
                " over due invoices and can be refreshed by pressing the Refresh Invoice Stats Button", "How to use this page");
        }
    }
}
